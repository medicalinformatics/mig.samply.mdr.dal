/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao.scratchpad;

import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.utils.Exporter;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.xsd.ObjectFactory;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import org.jooq.DSLContext;

/**
 *
 */
public class ExporterTest {

  public static void main(String[] args) {
    ResourceManager.initialize("localhost", "mdr", "paul", "");
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<IdentifiedElement> rootElements = IdentifiedDao.getRootElements(ctx, 0, "registry");

      Exporter exporter = new Exporter();

      for (IdentifiedElement element : rootElements) {
        exporter.add(element);
      }

      exporter.add(IdentifiedDao.getElement(ctx, 0, "urn:test-ns:catalog:1:1"));

      try {
        System.out.println(
            JAXBUtil.marshall(
                exporter.generateExport(ctx, 0, false), JAXBContext.newInstance(ObjectFactory.class)));

      } catch (JAXBException e) {
        e.printStackTrace();
      }
    }
  }

}
