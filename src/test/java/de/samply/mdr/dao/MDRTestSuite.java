/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao;

import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dao.subtests.DataElementGroupDAOTest;
import de.samply.mdr.dao.subtests.DescribedValueDomainDAOTest;
import de.samply.mdr.dao.subtests.EnumeratedValueDomainDAOTest;
import de.samply.mdr.dao.subtests.RecordEntryDAOTest;
import de.samply.mdr.dao.subtests.ScopedIdentifierDAOTest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.xml.bind.JAXBContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

//import de.samply.mdr.dao.subtests.DataElementDAOTest;

@Deprecated
@RunWith(Suite.class)
@Suite.SuiteClasses(
    {
        DescribedValueDomainDAOTest.class,
        EnumeratedValueDomainDAOTest.class,
        //DataElementDAOTest.class,
        ScopedIdentifierDAOTest.class,
        DataElementGroupDAOTest.class,
        RecordEntryDAOTest.class})
public class MDRTestSuite {

  private static Postgresql psql;

  @BeforeClass
  public static void setup() throws Exception {
    JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);

    Configurator.initialize("de.samply.mdr.test", "conf/log4j2.xml");
    psql = JAXBUtil.findUnmarshall("tests.mdr.dal.postgres.xml", context, Postgresql.class);
    ResourceManager.initialize(
        psql.getHost(), psql.getDatabase(), psql.getUsername(), psql.getPassword());

    /*
     * Create the database... this is a test-database, that we should drop afterwards...
     */
    createDatabase(psql);

// TODO: this wouldn't work anyway, mdr.sql is not available or am I missing something?
//
//      mdr.executeFile("src/main/resources/sql/mdr.sql");
//
//      Upgrader upgrade = new Upgrader(mdr);
//      upgrade.upgrade(false);
//
//      mdr.executeFile("src/main/resources/sql/tests.sql");
//
//      mdr.checkDbVersion();
//
//            Namespace ns = new Namespace();
//            ns.setCreatedBy(1);
//            ns.setDefinitions(new ArrayList<Definition>());
//            ns.setHidden(false);
//            ns.setName("mdr");
//
//            NamespaceDao.saveNamespace(ns);
//
//      mdr.commit();
  }

  /**
   * @throws SQLException
   */
  private static void createDatabase(Postgresql psql) throws SQLException {
    String newDatabase = psql.getDatabase() + "-test";

    String url = "jdbc:postgresql://" + psql.getHost() + "/" + psql.getDatabase();
    Connection conn = DriverManager.getConnection(url, psql.getUsername(), psql.getPassword());
    Statement stmt = conn.createStatement();
    stmt.executeUpdate("DROP DATABASE IF EXISTS \"" + newDatabase + "\"");
    stmt.executeUpdate("CREATE DATABASE \"" + newDatabase + "\"");
    stmt.close();
    conn.close();

    psql.setDatabase(newDatabase);
  }

}
