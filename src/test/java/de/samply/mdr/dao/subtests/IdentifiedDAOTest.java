/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dto.IdentifiedElement;
import java.io.FileNotFoundException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.jooq.DSLContext;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

public class IdentifiedDAOTest {

  /** These values differ from instance to instance...so make sure to find some viable codes and
   values
   */
  private static final String CATALOG_URN = "urn:test:dataelement:119:1";
  private static final String CODE1_URN = "urn:test:code:9588:1";
  private static final String CODE1_VALUE = "AL";
  private static final String CODE2_VALUE = "NL";
  private static Postgresql psql;

  @BeforeClass
  public static void init()
      throws JAXBException, FileNotFoundException, SAXException, ParserConfigurationException {
    JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
    psql = JAXBUtil.findUnmarshall("mdr.postgres.xml", context, Postgresql.class);
  }

  @Test
  public void getCode() {
    IdentifiedElement codeByUrn = getByUrn(CODE1_URN);
    IdentifiedElement codeByValue = getByValue(CATALOG_URN, CODE1_VALUE);
    IdentifiedElement codeByValue2 = getByValue(CATALOG_URN, CODE2_VALUE);

    assertNotNull(codeByUrn);
    assertNotNull(codeByValue);
    assertNotNull(codeByValue2);
    assertEquals(codeByUrn.getId(), codeByValue.getId());
    assertNotEquals(codeByUrn.getId(), codeByValue2.getId());
  }

  private IdentifiedElement getByUrn(String urn) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      return IdentifiedDao.getElement(ctx, 0, urn);
    }
  }

  private IdentifiedElement getByValue(String catalogUrn, String code) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      return IdentifiedDao.getElementByCode(ctx, 0, catalogUrn, code);
    }
  }
}
