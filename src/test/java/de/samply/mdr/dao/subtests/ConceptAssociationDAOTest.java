/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao.subtests;

import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ConceptAssociationDao;
import de.samply.mdr.dal.dto.ConceptAssociation;
import java.io.FileNotFoundException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.jooq.DSLContext;
import org.junit.Test;
import org.xml.sax.SAXException;

public class ConceptAssociationDAOTest extends AbstractTest {

  @Test
  public void insertAndFind() {
    ConceptAssociation conceptAssociation = new ConceptAssociation();
    conceptAssociation.setSystem("UMLS");
    conceptAssociation.setSource("userdefinded");
    conceptAssociation.setVersion("1.0");
    conceptAssociation.setTerm("Code");
    conceptAssociation.setText("Test");
    conceptAssociation.setLinktype("LinkTypeTest");
    conceptAssociation.setScopedIdentifierId(1);

    JAXBContext context = null;
    try {
      context = JAXBContext.newInstance(ObjectFactory.class);
      Postgresql psql = JAXBUtil
          .findUnmarshall("tests.mdr.dal.postgres.xml", context, Postgresql.class);

      ResourceManager.initialize(
          psql.getHost(), psql.getDatabase(), psql.getUsername(), psql.getPassword());
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        ConceptAssociationDao.saveConceptAssociation(ctx, conceptAssociation);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (JAXBException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }
}
