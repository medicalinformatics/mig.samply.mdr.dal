/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import org.jooq.DSLContext;
import org.junit.Test;

public class EnumeratedValueDomainDAOTest extends AbstractTest {

  @Test
  public void insertAndFind() {
    EnumeratedValueDomain domain = new EnumeratedValueDomain();
    domain.setDatatype("char");
    domain.setFormat("c");
    domain.setMaxCharacters(1);
    domain.setUnitOfMeasure("none");

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ElementDao.saveElement(ctx, 0, domain);
      //        assertTrue(domain.equals(dao.getEnumeratedValueDomain(domain.getId())));

      DataElement element = new DataElement();
      element.setValueDomainId(domain.getId());

      ElementDao.saveElement(ctx, 0, element);

      ScopedIdentifier identifier = new ScopedIdentifier();
      identifier.setStatus(Status.RELEASED);
      identifier.setVersion("1.0");
      identifier.setUrl("http://none.org");
      identifier.setNamespaceId(ElementDao.getNamespaces(ctx).get(0).getId());
      identifier.setElementId(element.getId());
      identifier.setIdentifier("" + element.getId());
      identifier.setElementType(Elementtype.DATAELEMENT);
      identifier.setCreatedBy(1);

      identifier.setId(ScopedIdentifierDao.saveScopedIdentifier(ctx, identifier));

      Definition elementDef = new Definition();
      elementDef.setDefinition("Das Genotypische Geschlecht des Patienten");
      elementDef.setDesignation("Geschlecht");
      elementDef.setLanguage("de");
      elementDef.setElementId(element.getId());
      elementDef.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, elementDef);

      elementDef = new Definition();
      elementDef.setDefinition("The patients sex");
      elementDef.setDesignation("Sex");
      elementDef.setLanguage("en");
      elementDef.setElementId(element.getId());
      elementDef.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, elementDef);

      PermissibleValue value = new PermissibleValue();
      value.setValueDomainId(domain.getId());
      value.setPermittedValue("m");

      ElementDao.saveElement(ctx, 0, value);
      assertTrue(value.equals(ElementDao.getElement(ctx, value.getId())));
      assertTrue(ElementDao.getPermissibleValues(ctx, domain.getId()).size() == 1);
      assertTrue(ElementDao.getPermissibleValues(ctx, domain.getId()).contains(value));

      Definition definition = new Definition();
      definition.setDefinition("Classifies the patients gender as male");
      definition.setDesignation("Male");
      definition.setLanguage("en");
      definition.setElementId(value.getId());
      definition.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, definition);

      definition = new Definition();
      definition.setDefinition(
          "Klassifiziert das Geschlecht des Patienten als genotypisch männlich");
      definition.setDesignation("Männlich");
      definition.setLanguage("de");
      definition.setElementId(value.getId());
      definition.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, definition);

      value = new PermissibleValue();
      value.setValueDomainId(domain.getId());
      value.setPermittedValue("f");

      ElementDao.saveElement(ctx, 0, value);
      assertTrue(value.equals(ElementDao.getElement(ctx, value.getId())));
      assertTrue(ElementDao.getPermissibleValues(ctx, domain.getId()).size() == 2);
      assertTrue(ElementDao.getPermissibleValues(ctx, domain.getId()).contains(value));

      definition = new Definition();
      definition.setDefinition("Classifies the patients gender as female");
      definition.setDesignation("Female");
      definition.setLanguage("en");
      definition.setElementId(value.getId());
      definition.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, definition);

      definition = new Definition();
      definition.setDefinition(
          "Klassifiziert das Geschlecht des Patienten als genotypisch weiblich");
      definition.setDesignation("Weiblich");
      definition.setLanguage("de");
      definition.setElementId(value.getId());
      definition.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, definition);

      assertTrue(definition.equals(DefinitionDao.getDefinition(ctx, definition.getId())));
      assertTrue(
          DefinitionDao.getDefinitions(ctx, definition.getElementId(), identifier.getId()).size() == 2);
      assertTrue(
          DefinitionDao.getDefinitions(ctx, definition.getElementId(), identifier.getId())
              .contains(definition));
      assertTrue(
          DefinitionDao.getDefinitions(ctx, definition.getElementId(), identifier.getId(), "de").size()
              == 1);
      assertTrue(
          DefinitionDao.getDefinitions(ctx, definition.getElementId(), identifier.getId(), "de")
              .contains(definition));
    }

  }
}
