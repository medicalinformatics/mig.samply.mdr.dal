/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao.subtests;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dto.Namespace;
import org.apache.logging.log4j.core.config.Configurator;
import org.jooq.DSLContext;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public abstract class AbstractTest {

  static {
    Configurator.initialize(null, "conf/log4j.xml");
  }

  @Rule
  public ExpectedException exception = ExpectedException.none();

  protected Namespace getNamespace() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      return (Namespace) ElementDao.getNamespaces(ctx).get(0);
    }
  }
}
