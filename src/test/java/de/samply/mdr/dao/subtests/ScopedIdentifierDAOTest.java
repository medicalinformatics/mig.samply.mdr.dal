/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dal.jooq.enums.Validationtype;
import org.jooq.DSLContext;
import org.junit.Test;

public class ScopedIdentifierDAOTest extends AbstractTest {

  @Test
  public void insertAndFind() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      DescribedValueDomain domain = new DescribedValueDomain();
      domain.setDatatype("DOUBLE");
      domain.setDescription("[0-9]*f");
      domain.setUnitOfMeasure("mg");
      domain.setFormat("no format");
      domain.setValidationType(Validationtype.NONE);

      ElementDao.saveElement(ctx, 0, domain);

      DataElement element = new DataElement();
      element.setValueDomainId(domain.getId());

      ElementDao.saveElement(ctx, 0, element);

      Element namespace = ElementDao.getNamespaces(ctx).get(0);

      ScopedIdentifier identifier = new ScopedIdentifier();
      identifier.setStatus(Status.RELEASED);
      identifier.setNamespaceId(namespace.getId());
      identifier.setUrl("http://this.does.not.exists");
      identifier.setVersion("1.2e");
      identifier.setElementId(element.getId());
      identifier.setIdentifier("" + element.getId());
      identifier.setElementType(Elementtype.DATAELEMENT);
      identifier.setNamespace("mdr-test");
      identifier.setCreatedBy(1);

      identifier.setId(ScopedIdentifierDao.saveScopedIdentifier(ctx, identifier));

      assertTrue(ScopedIdentifierDao.getScopedIdentifier(ctx, identifier.getId()).equals(identifier));
      DataElement e2 = (DataElement) ElementDao.getElement(ctx, element.getId());
      assertTrue(e2.equals(element));

      assertTrue(
          ScopedIdentifierDao.getIdenticalScopedIdentifier(
              ctx, identifier.toString(), identifier.getNamespace(), 0)
              .equals(identifier));
    }
  }

}
