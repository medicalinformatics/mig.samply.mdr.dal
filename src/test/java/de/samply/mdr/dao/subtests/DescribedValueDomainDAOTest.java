/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dal.jooq.enums.Validationtype;
import org.jooq.DSLContext;
import org.junit.Test;

public class DescribedValueDomainDAOTest extends AbstractTest {

  @Test
  public void insertAndFind() {
    DescribedValueDomain domain = new DescribedValueDomain();
    domain.setDatatype("integer");
    domain.setDescription("[0-9]+");
    domain.setMaxCharacters(1000);
    domain.setUnitOfMeasure("years");
    domain.setFormat("none");
    domain.setValidationType(Validationtype.NONE);

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ElementDao.saveElement(ctx, 0, domain);

      DescribedValueDomain describedValue =
          (DescribedValueDomain) ElementDao.getElement(ctx, domain.getId());

      assertTrue(describedValue.equals(domain));

      DataElement element = new DataElement();
      element.setValueDomainId(describedValue.getId());

      ElementDao.saveElement(ctx, 0, element);

      ScopedIdentifier identifier = new ScopedIdentifier();
      identifier.setStatus(Status.RELEASED);
      identifier.setUrl("http://NONE");
      identifier.setVersion("1.0");
      identifier.setNamespaceId(getNamespace().getId());
      identifier.setElementId(element.getId());
      identifier.setIdentifier("" + element.getId());
      identifier.setElementType(Elementtype.DATAELEMENT);
      identifier.setCreatedBy(1);

      identifier.setId(ScopedIdentifierDao.saveScopedIdentifier(ctx, identifier));

      Definition d = new Definition();
      d.setElementId(describedValue.getId());
      d.setDefinition("Eine ganze Zahl");
      d.setDesignation("none");
      d.setLanguage("de");
      d.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, d);
      assertTrue(DefinitionDao.getDefinition(ctx, d.getId()).equals(d));

      d = new Definition();
      d.setElementId(describedValue.getId());
      d.setDesignation("none");
      d.setDefinition("An integer");
      d.setLanguage("en");
      d.setScopedIdentifierId(identifier.getId());

      DefinitionDao.saveDefinition(ctx, d);

      assertTrue(DefinitionDao.getDefinition(ctx, d.getId()).equals(d));
      assertTrue(
          DefinitionDao.getDefinitions(ctx, describedValue.getId(), identifier.getId()).size() == 2);

      d = new Definition();
      d.setElementId(element.getId());
      d.setDesignation("Patients age");
      d.setDefinition("The patients age at time of death in years");
      d.setLanguage("en");
      d.setScopedIdentifierId(identifier.getId());
      DefinitionDao.saveDefinition(ctx, d);

      d = new Definition();
      d.setElementId(element.getId());
      d.setDesignation("Alter des Patienten");
      d.setDefinition("Das Alter des Patienten zum Zeitpunkt des Todes");
      d.setLanguage("de");
      d.setScopedIdentifierId(identifier.getId());
      DefinitionDao.saveDefinition(ctx, d);
    }

  }

}
