/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.db.migration;

import static de.samply.mdr.dal.jooq.Tables.IMPORT;
import static de.samply.mdr.dal.jooq.Tables.STAGING;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.jooq.impl.DSL;

public class V14_2__GroupStagingByImports extends BaseJavaMigration {

  private static final String GENERIC_IMPORT_LABEL = "Unnamed Import";

  public void migrate(Context context) throws Exception {
    String query = "SELECT id, created_by, namespace_id FROM staging";
    List<MovedReference> movedReferences = DSL.using(context.getConnection()).fetch(query)
        .map(rs -> new MovedReference(
            rs.get("id", Integer.class),
            rs.get("created_by", Integer.class),
            rs.get("namespace_id", Integer.class)));
    Map<CreatorNamespacePair, Integer> createdImports = new HashMap<>();

    // Create "legacy" imports for all combinations of users and namespaces
    for (MovedReference m : movedReferences) {
      if (!createdImports.containsKey(m.creatorNamespacePair)) {
        int id = DSL.using(context.getConnection())
            .insertInto(IMPORT,
                IMPORT.NAMESPACE_ID, IMPORT.CREATED_BY, IMPORT.LABEL, IMPORT.UUID)
            .values(m.creatorNamespacePair.getNamespace_id(),
                m.creatorNamespacePair.getCreated_by(),
                GENERIC_IMPORT_LABEL,
                UUID.randomUUID())
            .returning(IMPORT.ID)
            .fetchOne().getId();
        createdImports.put(m.creatorNamespacePair, id);
      }
    }

    // Update all entries with the import ids
    for (Map.Entry<CreatorNamespacePair, Integer> entry : createdImports.entrySet()) {
      DSL.using(context.getConnection())
          .update(STAGING)
          .set(STAGING.IMPORT_ID, entry.getValue())
          .where(STAGING.field("created_by", Integer.class).eq(entry.getKey().getCreated_by()))
          .and(STAGING.field("namespace_id", Integer.class).eq(entry.getKey().getNamespace_id()));
    }
  }

  private static class CreatorNamespacePair {

    private int created_by;
    private int namespace_id;

    public CreatorNamespacePair(int created_by, int namespace_id) {
      this.created_by = created_by;
      this.namespace_id = namespace_id;
    }

    public int getCreated_by() {
      return created_by;
    }

    public int getNamespace_id() {
      return namespace_id;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      CreatorNamespacePair that = (CreatorNamespacePair) o;
      return created_by == that.created_by && namespace_id == that.namespace_id;
    }

    @Override
    public int hashCode() {
      return Objects.hash(created_by, namespace_id);
    }
  }

  private static class MovedReference {

    private int id;
    private CreatorNamespacePair creatorNamespacePair;

    public MovedReference(int id, int created_by, int namespace_id) {
      this.id = id;
      this.creatorNamespacePair = new CreatorNamespacePair(created_by, namespace_id);
    }
  }
}
