/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.db.migration;

import static de.samply.mdr.dal.jooq.Tables.CONFIG;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.jooq.tables.pojos.Config;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.api.MigrationInfo;
import org.jooq.DSLContext;
import org.json.JSONObject;

public class MigrationUtil {

  /**
   * Apply all necessary database migrations via flyway
   *
   * <p>First check if an old configuration entry with a version is available. Set this as the
   * baseline version. Otherwise set the baseline version to 5, since the basic installation starts
   * on version 6. When migrating a non-empty database without flyway schema, this baseline version
   * is set. Otherwise, this is ignored.
   */
  public static void migrateDatabase() {
    int baselineVersion = isInstalled() ? getCurrentVersion() : 5;

    Flyway flyway =
        Flyway.configure()
            .baselineVersion(Integer.toString(baselineVersion))
            .baselineOnMigrate(true)
            .locations("classpath:de/samply/mdr/db/migration")
            .dataSource(ResourceManager.getUrl(),
                ResourceManager.getUser(),
                ResourceManager.getPassword())
            .load();

    /*
     * Check if there is a minor version for the baseline version. In this case, set the baseline
     * version to the newest
     *
     * <p>The old migration tool only knew major versions, however, multiple scripts could be
     * executed in one migration. Example: The upgrade to version 8 consisted of two sql scripts,
     * and in the end the version was "8". Now, this would lead to the version being "8.1". Flyway
     * interprets "8" as "8.0" and would now apply 8.1, which would not be the desired output
     *
     * <p>To avoid this, look through all available migrations and for those with the same major
     * number as the one from the config, check if there are scripts with a minor version. If so,
     * set this as the new baseline and re-init flyway.
     */
    String refinedBaselineVersion = null;
    for (MigrationInfo migrationInfo : flyway.info().all()) {
      if (migrationInfo
          .getVersion()
          .getMajorAsString()
          .equalsIgnoreCase(flyway.getConfiguration().getBaselineVersion().getMajorAsString())) {
        if (migrationInfo
            .getVersion()
            .isNewerThan(flyway.getConfiguration().getBaselineVersion().getVersion())) {
          refinedBaselineVersion = migrationInfo.getVersion().getVersion();
        }
      }
    }

    if (refinedBaselineVersion != null) {
      flyway =
          Flyway.configure()
              .baselineVersion(refinedBaselineVersion)
              .baselineOnMigrate(true)
              .locations("classpath:de/samply/mdr/db/migration")
              .dataSource(ResourceManager.getUrl(),
                  ResourceManager.getUser(),
                  ResourceManager.getPassword())
              .load();
    }

    flyway.migrate();
  }

  public static boolean isInstalled() {
    try {
      boolean ok = false;
      DatabaseMetaData metaData = ResourceManager.getConnection().getMetaData();
      ResultSet tables = metaData.getTables((String)null, (String)null, "config", (String[])null);
      if (tables.next()) {
        ok = true;
      }

      tables.close();
      return ok;
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
  }

  public static int getCurrentVersion() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      Config conf = ctx.fetchOne(CONFIG, CONFIG.NAME.eq("defaultConfig")).into(Config.class);
      return new JSONObject(conf.getValue().toString()).getInt("dbVersion");
    }
  }

  /** Checks the database version and returns true, if they are compatible. */
  public static boolean checkDbVersion() {
    try {
      Flyway flyway =
          Flyway.configure().dataSource(
              ResourceManager.getUrl(),
              ResourceManager.getUser(),
              ResourceManager.getPassword()).load();
      flyway.validate();
      return true;
    } catch (FlywayException e) {
      return false;
    }
  }

}
