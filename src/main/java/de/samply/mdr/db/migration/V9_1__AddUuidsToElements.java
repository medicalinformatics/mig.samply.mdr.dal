/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.db.migration;

import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIER;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.impl.DSL;

public class V9_1__AddUuidsToElements extends BaseJavaMigration {

  public void migrate(Context context) throws Exception {
    try(DSLContext ctx = DSL.using(context.getConnection())) {
      List<Integer> updateElements = ctx.select(SCOPEDIDENTIFIER.ID).from(SCOPEDIDENTIFIER)
          .fetch().stream().map(Record1::value1).collect(Collectors.toList());

      ctx.update(SCOPEDIDENTIFIER)
          .set(SCOPEDIDENTIFIER.UUID, UUID.randomUUID())
          .where(SCOPEDIDENTIFIER.ID.in(updateElements))
          .execute();

      updateElements = ctx.select(ELEMENT.ID).from(ELEMENT)
          .fetch().stream().map(Record1::value1).collect(Collectors.toList());

      ctx.update(ELEMENT)
          .set(ELEMENT.UUID, UUID.randomUUID())
          .where(ELEMENT.ID.in(updateElements))
          .execute();
    }
  }
}
