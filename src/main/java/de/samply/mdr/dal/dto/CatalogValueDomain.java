/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.tables.pojos.Element;

/** A value domain that is linked to a catalog (a scoped identifier for a catalog). */
public class CatalogValueDomain extends ValueDomain {

  private static final long serialVersionUID = -2811764103710495776L;

  private Integer catalogScopedIdentifierId; // SCOPED_ID

  public CatalogValueDomain() {
    super();
    setElementType(Elementtype.CATALOG_VALUE_DOMAIN);
  }

  public CatalogValueDomain(Element element) {
    super(element);
    setCatalogScopedIdentifierId(element.getScopedidentifierid());
  }

  public Integer getCatalogScopedIdentifierId() {
    return catalogScopedIdentifierId;
  }

  public void setCatalogScopedIdentifierId(Integer catalogScopedIdentifierId) {
    this.catalogScopedIdentifierId = catalogScopedIdentifierId;
  }

  /**
   * Converts a CatalogValueDomain dto of samply.mdr.dal to an Element dto of jooq.
   */
  @Override
  public Element toJooq() {
    Element element = super.toJooq();
    element.setScopedidentifierid(getCatalogScopedIdentifierId());
    return element;
  }

}
