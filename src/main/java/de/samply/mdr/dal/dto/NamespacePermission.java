/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.GrantType;
import de.samply.mdr.dal.jooq.tables.pojos.UserNamespaceGrants;
import java.io.Serializable;

/**
 * A namespace permission grants a user access to a certain namespace. The mode of access is
 * described by the permission field.
 */
public class NamespacePermission implements Serializable {

  private static final long serialVersionUID = -6494829167297801218L;

  public NamespacePermission() {
  }

  /**
   * Create a NamespacePermission dto of samply.mdr.dal from UserNamespaceGrants dto of jooq.
   */
  public NamespacePermission(UserNamespaceGrants grants) {
    setUserId(grants.getUserId());
    setNamespaceId(grants.getNamespaceId());
    setPermission(Permission.grantTypeToPermission(grants.getGrantType()));
  }

  private Integer userId;

  private Integer namespaceId;
  /** The mode of access. */
  private Permission permission;

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Permission getPermission() {
    return permission;
  }

  public void setPermission(Permission permission) {
    this.permission = permission;
  }

  public Integer getNamespaceId() {
    return namespaceId;
  }

  public void setNamespaceId(Integer namespaceId) {
    this.namespaceId = namespaceId;
  }

  /**
   * Converts a NamespacePermission dto of samply.mdr.dal to a UserNamespaceGrants to of jooq.
   */
  public UserNamespaceGrants toJooq() {
    UserNamespaceGrants grant = new UserNamespaceGrants();
    grant.setUserId(getUserId());
    grant.setNamespaceId(getNamespaceId());
    grant.setGrantType(Permission.permissionToGrantType(getPermission()));
    return grant;
  }

  /** The permission enum that describes the mode of access. */
  public enum Permission {
    READ,
    READ_WRITE,
    READ_WRITE_ADMIN;

    public String getName() {
      return toString();
    }

    /**
     * Convert a JOOQ GrantType to a Permission.
     */
    public static Permission grantTypeToPermission(GrantType grantType) {
      // TODO: it would be WAY better to change permission to GrantType.
      switch (grantType) {
        case READ:
          return Permission.READ;
        case WRITE:
          return Permission.READ_WRITE;
        case ADMIN:
          return Permission.READ_WRITE_ADMIN;
        default:
          return null;
      }
    }

    /**
     * Convert a Permission to a JOOQ GrantType.
     */
    public static GrantType permissionToGrantType(Permission permission) {
      // TODO: see grantTypeToPermission method, this should be changed.
      switch (permission) {
        case READ:
          return GrantType.READ;
        case READ_WRITE:
          return GrantType.WRITE;
        case READ_WRITE_ADMIN:
          return GrantType.ADMIN;
        default:
          return null;
      }
    }
  }
}
