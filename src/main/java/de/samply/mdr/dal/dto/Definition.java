/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import java.io.Serializable;

/**
 * A definition plus designation in a specific language for an element in the context of a scoped
 * identifier (optional).
 *
 * @author paul
 */
public class Definition implements Serializable {

  private static final long serialVersionUID = -218608973358407005L;

  private Integer id;
  /** The designation (a label or a picture). */
  private String designation;
  /** A definition that describes the element item more thoroughly. */
  private String definition;
  /** The language (code?), e.g. "de". */
  private String language;

  private Integer scopedIdentifierId;

  private Integer elementId;

  public Definition() {
  }

  /**
   * Create a Definition dto of samply.mdr.dal from Definition dto of jooq.
   */
  public Definition(de.samply.mdr.dal.jooq.tables.pojos.Definition definition) {
    setId(definition.getId());
    setScopedIdentifierId(definition.getScopedidentifierid());
    setElementId(definition.getElementid());
    setDefinition(definition.getDefinition());
    setDesignation(definition.getDesignation());
    setLanguage(definition.getLanguage());
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public Integer getElementId() {
    return elementId;
  }

  public Integer setElementId(Integer elementId) {
    this.elementId = elementId;
    return elementId;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Definition)) {
      return false;
    }
    Definition d = (Definition) obj;
    return d.id == id
        && d.definition.equals(definition)
        && d.designation.equals(designation)
        && d.language.equals(language)
        && d.getScopedIdentifierId() == getScopedIdentifierId()
        && d.elementId == elementId;
  }

  public Integer getScopedIdentifierId() {
    return scopedIdentifierId;
  }

  public void setScopedIdentifierId(Integer scopedIdentifierId) {
    this.scopedIdentifierId = scopedIdentifierId;
  }

  /**
   * Convert a Definition dto of samply.mdr.dal to a Definition to of jooq.
   */
  public de.samply.mdr.dal.jooq.tables.pojos.Definition toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Definition definition =
        new de.samply.mdr.dal.jooq.tables.pojos.Definition();

    definition.setId(getId());
    definition.setScopedidentifierid(getScopedIdentifierId());
    definition.setLanguage(getLanguage());
    definition.setElementid(getElementId());
    definition.setDesignation(getDesignation());
    definition.setDefinition(getDefinition());

    return definition;
  }
}
