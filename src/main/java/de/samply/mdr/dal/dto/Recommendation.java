/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Recommendation implements Serializable {

  private String designation;
  private String definition;
  private String language;
  private String type;
  private int designationSimilarity;
  private int definitionSimilarity;
  private List<Definition> definitions;
  private String urn;

  public Recommendation() {
    definitions = new ArrayList<>();
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public float getDesignationSimilarity() {
    return designationSimilarity;
  }

  public void setDesignationSimilarity(int designationSimilarity) {
    this.designationSimilarity = designationSimilarity;
  }

  public float getDefinitionSimilarity() {
    return definitionSimilarity;
  }

  public void setDefinitionSimilarity(int definitionSimilarity) {
    this.definitionSimilarity = definitionSimilarity;
  }

  public List<Definition> getDefinitions() {
    return definitions;
  }

  public void setDefinitions(List<Definition> definitions) {
    this.definitions = definitions;
  }

  public String getUrn() {
    return urn;
  }

  public void setUrn(String urn) {
    this.urn = urn;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Recommendation that = (Recommendation) o;
    return Objects.equals(urn, that.urn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(urn);
  }

  public static class Definition implements Serializable {

    private String designation;
    private String definition;
    private String language;

    /**
     * TODO: add javadoc.
     */
    public Definition(Recommendation r) {
      this.designation = r.designation;
      this.definition = r.definition;
      this.language = r.language;
    }

    public String getDesignation() {
      return designation;
    }

    public String getDefinition() {
      return definition;
    }

    public String getLanguage() {
      return language;
    }
  }
}
