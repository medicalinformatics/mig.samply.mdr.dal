/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Validationtype;
import de.samply.mdr.dal.jooq.tables.pojos.Element;

/**
 * A described value domain can be used for validation with a regular expression, an integer within
 * a range, etc.
 */
public class DescribedValueDomain extends ValueDomain {

  private static final long serialVersionUID = 3436937687231210778L;
  private String description;
  private Validationtype validationType;
  /** The validationData depends on the validation type. */
  private String validationData;

  public DescribedValueDomain() {
    super();
    setElementType(Elementtype.DESCRIBED_VALUE_DOMAIN);
  }

  /**
   * Create an DescribedValueDomain dto of samply.mdr.dal from an Element dto of jooq.
   */
  public DescribedValueDomain(Element element) {
    super(element);
    setDescription(element.getDescription());
    setValidationData(element.getValidationdata());
    setValidationType(element.getValidationtype());
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof DescribedValueDomain)) {
      return false;
    }
    DescribedValueDomain ds = (DescribedValueDomain) obj;
    return super.equals(obj) && description.equals(ds.description);
  }

  public String getValidationData() {
    return validationData;
  }

  public void setValidationData(String validationData) {
    this.validationData = validationData;
  }

  public Validationtype getValidationType() {
    return validationType;
  }

  public void setValidationType(Validationtype validationType) {
    this.validationType = validationType;
  }

  /**
   * Converts a DescribedValueDomain dto of samply.mdr.dal to an Element dto of jooq.
   */
  @Override
  public Element toJooq() {
    Element element = super.toJooq();
    element.setDescription(getDescription());
    element.setValidationtype(getValidationType());
    element.setValidationdata(getValidationData());
    return element;
  }
}
