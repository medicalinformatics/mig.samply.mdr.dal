/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;
import java.util.List;

/**
 * An identifier element contains, a scoped identifier, the element itself, the descriptions for
 * this element in the context of the scoped identifier.
 */
public class IdentifiedElement extends DescribedElement {

  private static final long serialVersionUID = 8177831717232234676L;

  public IdentifiedElement() {
    super();
  }

  public IdentifiedElement(ScopedIdentifier scoped) {
    super();
    this.scoped = scoped;
  }

  public IdentifiedElement(List<Definition> definitions, Element element, ScopedIdentifier scoped) {
    super(definitions, element);
    this.scoped = scoped;
  }

  /** The Scoped Identifier for this element. */
  private ScopedIdentifier scoped;

  public ScopedIdentifier getScoped() {
    return scoped;
  }

  public void setScoped(ScopedIdentifier scoped) {
    this.scoped = scoped;
  }

  public Elementtype getElementType() {
    return getScoped().getElementType();
  }

  public int getId() {
    return getElement().getId();
  }
}
