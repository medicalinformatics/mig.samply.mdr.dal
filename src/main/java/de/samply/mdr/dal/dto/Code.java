/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;

/** A code from a catalog, e.g. "C00.1" from the ICD-10 code. */
public class Code extends Element {

  private static final long serialVersionUID = -1123637844900682521L;
  private Integer catalogId; // ELEMENT_ID
  /** The code itself, e.g. "C00.1" */
  private String code;
  /**
   * If false, this code is just a hierarchical node and must not be used as a valid code for user
   * selection. Chapters and Blocks from the ICD-10 catalog are not valid codes, but hierarchical
   * nodes.
   */
  private boolean isValid;

  public Code() {
    super();
    setElementType(Elementtype.CODE);
  }

  /**
   * Create an Code dto of samply.mdr.dal from an Element dto of jooq.
   */
  public Code(de.samply.mdr.dal.jooq.tables.pojos.Element element) {
    super(element);
    setCode(element.getCode());
    setCatalogId(element.getElementid());
    setValid(element.getIsvalid());
  }

  public Integer getCatalogId() {
    return catalogId;
  }

  public void setCatalogId(Integer catalogId) {
    this.catalogId = catalogId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public boolean isValid() {
    return isValid;
  }

  public void setValid(boolean isValid) {
    this.isValid = isValid;
  }

  /**
   * Converts a Code dto of samply.mdr.dal to an Element dto of jooq.
   */
  @Override
  public de.samply.mdr.dal.jooq.tables.pojos.Element toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Element element = super.toJooq();
    element.setCode(getCode());
    element.setIsvalid(isValid());
    element.setElementid(getCatalogId());
    return element;
  }
}
