/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

public class Import implements Serializable {

  private Integer id;
  private Timestamp createdAt;
  private Integer createdBy;
  private Timestamp convertedAt;
  private Integer namespaceId;
  private String source;
  private String label;
  private UUID uuid;

  public Import() {
  }

  /**
   * Create an Import dto of samply.mdr.dal from Import dto of jooq.
   */
  public Import(de.samply.mdr.dal.jooq.tables.pojos.Import theImport) {
    setId(theImport.getId());
    setConvertedAt(theImport.getConvertedAt());
    setCreatedAt(theImport.getCreatedAt());
    setCreatedBy(theImport.getCreatedBy());
    setLabel(theImport.getLabel());
    setNamespaceId(theImport.getNamespaceId());
    setSource(theImport.getSource());
    setUuid(theImport.getUuid());
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Integer getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Integer createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getConvertedAt() {
    return convertedAt;
  }

  public void setConvertedAt(Timestamp convertedAt) {
    this.convertedAt = convertedAt;
  }

  public Integer getNamespaceId() {
    return namespaceId;
  }

  public void setNamespaceId(Integer namespaceId) {
    this.namespaceId = namespaceId;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  /**
   * Converts an Import dto of samply.mdr.dal to an Import dto of jooq.
   */
  public de.samply.mdr.dal.jooq.tables.pojos.Import toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Import theImport =
        new de.samply.mdr.dal.jooq.tables.pojos.Import();

    theImport.setId(getId());
    theImport.setConvertedAt(getConvertedAt());
    theImport.setCreatedAt(getCreatedAt());
    theImport.setCreatedBy(getCreatedBy());
    theImport.setLabel(getLabel());
    theImport.setNamespaceId(getNamespaceId());
    theImport.setSource(getSource());
    theImport.setUuid(getUuid());

    return theImport;
  }
}
