/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;

/** A dataelement. */
public class DataElement extends Element {

  private static final long serialVersionUID = 692034583264389579L;

  private Integer valueDomainId; // Element ID

  public DataElement() {
    super();
    setElementType(Elementtype.DATAELEMENT);
  }

  public DataElement(de.samply.mdr.dal.jooq.tables.pojos.Element element) {
    super(element);
    setValueDomainId(element.getElementid());
  }

  public Integer getValueDomainId() {
    return valueDomainId;
  }

  public void setValueDomainId(Integer valueDomainId) {
    this.valueDomainId = valueDomainId;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof DataElement)) {
      return false;
    }
    DataElement e = (DataElement) obj;
    return super.equals(obj) && e.valueDomainId == valueDomainId;
  }

  /**
   * Converts a DataElement dto of samply.mdr.dal to an Element dto of jooq.
   */
  @Override
  public de.samply.mdr.dal.jooq.tables.pojos.Element toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Element element = super.toJooq();
    element.setElementid(getValueDomainId());
    return element;
  }

}
