/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.tables.pojos.Mdruser;
import de.samply.string.util.StringUtil;
import java.io.Serializable;
import org.jooq.JSON;
import org.json.JSONObject;

public class User implements Serializable {

  public static final String JSON_FIELD_EMAIL = "email";
  public static final String JSON_FIELD_REALNAME = "realName";
  public static final String JSON_FIELD_EXTERNAL_LABEL = "externalLabel";

  private static final long serialVersionUID = -5716614960196150909L;
  /** The id in the database. */
  private Integer id;
  /** The username, usually the samply auth subject. */
  private String username;
  /**
   * The data column contains various information about the user, e.g. his real name or the label of
   * the external identity provider.
   */
  private JSONObject data;

  public User() {
    data = new JSONObject();
  }

  /**
   * Create an User of samply.mdr.dal from an Mdruser of jooq.
   */
  public User(Mdruser mdruser) {
    setId(mdruser.getId());
    setData(new JSONObject(mdruser.getData().data()));
    setUsername(mdruser.getUsername());
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public JSONObject getData() {
    return data;
  }

  public void setData(JSONObject data) {
    this.data = data;
  }

  /** Returns the users email address. */
  public String getEmail() {
    if (data.get(JSON_FIELD_EMAIL) != null) {
      return (String) data.get(JSON_FIELD_EMAIL);
    } else {
      return null;
    }
  }

  /** Sets the users email address. */
  public void setEmail(String email) {
    data.put(JSON_FIELD_EMAIL, email);
  }

  /** Returns the users real name. */
  public String getRealName() {
    return (String) data.get(JSON_FIELD_REALNAME);
  }

  /** Sets the users real name. */
  public void setRealName(String realName) {
    data.put(JSON_FIELD_REALNAME, realName);
  }

  /** Returns the label for the external identity provider. */
  public String getExternalLabel() {
    return data.has(JSON_FIELD_EXTERNAL_LABEL)
        ? (String) data.get(JSON_FIELD_EXTERNAL_LABEL)
        : null;
  }

  /** Sets the label for the external identity provider. */
  public void setExternalLabel(String label) {
    if (StringUtil.isEmpty(label)) {
      data.remove(JSON_FIELD_EXTERNAL_LABEL);
    } else {
      data.put(JSON_FIELD_EXTERNAL_LABEL, label);
    }
  }

  /**
   * Converts an User dto of samply.mdr.dal to a Mdruser of jooq.
   */
  public Mdruser toJooq() {
    Mdruser mdruser = new Mdruser();
    mdruser.setId(getId());
    mdruser.setUsername(getUsername());
    mdruser.setData(JSON.valueOf(getData().toString()));
    return mdruser;
  }
}
