/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;
import java.io.Serializable;
import java.util.UUID;

public abstract class Element implements Serializable {

  private static final long serialVersionUID = 1623254589015110402L;
  private Integer id;
  /** The element type of this element. */
  private Elementtype elementType;
  /** The UUID of this element. */
  private UUID uuid;
  /** The external ID of this element, e.g. "cadsr://dataelement?publicId=abc". */
  private String externalId;

  public Element() {
    setUuid(UUID.randomUUID());
  }

  /**
   * Create an Element dto of samply.mdr.dal from an Element dto of jooq.
   */
  public Element(de.samply.mdr.dal.jooq.tables.pojos.Element element) {
    setId(element.getId());
    setElementType(element.getElementtype());
    setExternalId(element.getExternalid());
    setUuid(element.getUuid());
  }

  public Elementtype getElementType() {
    return elementType;
  }

  public void setElementType(Elementtype elementType) {
    this.elementType = elementType;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Element)) {
      return false;
    }
    Element e = (Element) obj;
    return id == e.id;
  }

  @Override
  public int hashCode() {
    return Integer.valueOf(id).hashCode();
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }

  /**
   * Converts a Element dto of samply.mdr.dal to an Element dto of jooq.
   */
  public de.samply.mdr.dal.jooq.tables.pojos.Element toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Element element =
        new de.samply.mdr.dal.jooq.tables.pojos.Element();

    element.setId(getId());
    element.setElementtype(getElementType());
    element.setExternalid(getExternalId());
    element.setUuid(getUuid());

    return element;
  }
}
