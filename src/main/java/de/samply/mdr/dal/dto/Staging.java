/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;
import java.io.Serializable;
import java.util.Objects;

public class Staging implements Serializable {

  private Integer id;
  private Integer importId;
  private Integer parentId;
  private String data;
  private Integer elementId;
  /** The element type of this element. */
  private Elementtype elementType;

  private String designation;

  public Staging() {
  }

  /**
   * Create a Staging dto of samply.mdr.dal from Staging dto of jooq.
   */
  public Staging(de.samply.mdr.dal.jooq.tables.pojos.Staging staging) {
    setId(staging.getId());
    setImportId(staging.getImportId());
    if (staging.getParentId() != null) {
      setParentId(staging.getParentId());
    }
    setData(staging.getData());
    if (staging.getElementId() != null) {
      setElementId(staging.getElementId());
    }
    setElementType(staging.getElementType());
    setDesignation(staging.getDesignation());
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getImportId() {
    return importId;
  }

  public void setImportId(Integer importId) {
    this.importId = importId;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Elementtype getElementType() {
    return elementType;
  }

  public void setElementType(Elementtype elementType) {
    this.elementType = elementType;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public Integer getElementId() {
    return elementId;
  }

  public void setElementId(Integer elementId) {
    this.elementId = elementId;
  }

  /**
   * Converts a Staging dto of samply.mdr.dal to a Staging to of jooq.
   */
  public de.samply.mdr.dal.jooq.tables.pojos.Staging toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Staging staging =
        new de.samply.mdr.dal.jooq.tables.pojos.Staging();

    staging.setId(getId());
    staging.setData(getData());
    staging.setDesignation(getDesignation());
    staging.setElementId(getElementId());
    staging.setElementType(getElementType());
    staging.setImportId(getImportId());
    staging.setParentId(getParentId());

    return staging;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Staging that = (Staging) o;
    return id == that.id;
  }

  @Override
  public int hashCode() {

    return Objects.hash(id);
  }
}
