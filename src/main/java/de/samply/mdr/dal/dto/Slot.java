/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import java.io.Serializable;

/**
 * A slot (a key value pair).
 *
 * @author paul
 */
public class Slot implements Serializable {

  private static final long serialVersionUID = 2922421882685775421L;
  private Integer id;
  private String key;
  private String value;
  private Integer scopedIdentifierId;

  public Slot() {
  }

  /**
   * Create a Slot dto of samply.mdr.dal from Slot dto of jooq.
   */
  public Slot(de.samply.mdr.dal.jooq.tables.pojos.Slot slot) {
    setId(slot.getId());
    setKey(slot.getKey());
    setValue(slot.getValue());
    setScopedIdentifierId(slot.getScopedidentifierid());
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Integer getScopedIdentifierId() {
    return scopedIdentifierId;
  }

  public void setScopedIdentifierId(Integer scopedIdentifierId) {
    this.scopedIdentifierId = scopedIdentifierId;
  }

  /**
   * Convert a Slot dto of samply.mdr.dal to a Slot to of jooq.
   */
  public de.samply.mdr.dal.jooq.tables.pojos.Slot toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Slot slot = new de.samply.mdr.dal.jooq.tables.pojos.Slot();
    slot.setId(getId());
    slot.setKey(getKey());
    slot.setValue(getValue());
    slot.setScopedidentifierid(getScopedIdentifierId());
    return slot;
  }
}
