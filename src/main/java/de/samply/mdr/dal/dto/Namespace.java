/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jooq.JSON;
import org.json.JSONObject;

/**
 * The namespace inside a MDR source.
 *
 * @author paul
 */
public class Namespace extends Element {

  public static final String JSON_FIELD_CONSTRAINTS = "constraints";
  public static final String JSON_FIELD_LANGUAGES = "languages";
  public static final String JSON_FIELD_SLOTS = "slots";

  private static final long serialVersionUID = 3341526117020615061L;
  /** The name. */
  private String name;
  /** The users ID, who has created this namespace. */
  private Integer createdBy;
  /** If the namespace is hidden, it is only visible to certain users. */
  private Boolean hidden = false;
  /** The additional data column, e.g. constraints. */
  private JSONObject data = new JSONObject();

  public Namespace() {
    super();
    setElementType(Elementtype.NAMESPACE);
  }

  /**
   * Create a Namespace dto of samply.mdr.dal from Element dto of jooq.
   */
  public Namespace(de.samply.mdr.dal.jooq.tables.pojos.Element element) {
    super(element);
    setCreatedBy(element.getCreatedby());
    setData(new JSONObject(element.getData().data()));
    setHidden(element.getHidden());
    setName(element.getName());
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Namespace)) {
      return false;
    }
    Namespace n = (Namespace) obj;
    return super.equals(obj) && n.name.equals(name);
  }

  public Integer getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Integer createdBy) {
    this.createdBy = createdBy;
  }

  public Boolean getHidden() {
    return hidden;
  }

  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  public JSONObject getData() {
    return data;
  }

  public void setData(JSONObject data) {
    this.data = data;
  }

  /**
   * TODO: add javadoc.
   */
  public JSONObject getConstraints() {
    if (data.has(JSON_FIELD_CONSTRAINTS)) {
      return data.getJSONObject(JSON_FIELD_CONSTRAINTS);
    } else {
      return new JSONObject();
    }
  }

  /**
   * Returns a list of languages for a namespace.
   */
  public List<String> getLanguages() {
    List<String> languages = new ArrayList<>();
    if (getConstraints().has(JSON_FIELD_LANGUAGES)) {
      for (Object value : getConstraints().getJSONArray(JSON_FIELD_LANGUAGES)) {
        languages.add(value.toString());
      }
      return languages;
    } else {
      return Collections.emptyList();
    }
  }

  /**
   * Returns a list of slots for a namespace.
   */
  public List<String> getConstraintsSlots() {
    List<String> slots = new ArrayList<>();
    if (getConstraints().has(JSON_FIELD_SLOTS)) {
      for (Object value : getConstraints().getJSONArray(JSON_FIELD_SLOTS)) {
        slots.add(value.toString());
      }
      return slots;
    } else {
      return Collections.emptyList();
    }
  }

  /**
   * Converts a Namespace dto of samply.mdr.dal to a Element to of jooq.
   */
  @Override
  public de.samply.mdr.dal.jooq.tables.pojos.Element toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Element element = super.toJooq();
    element.setName(getName());
    element.setData(JSON.valueOf(getData().toString()));
    element.setHidden(getHidden());
    element.setCreatedby(getCreatedBy());
    return element;
  }
}
