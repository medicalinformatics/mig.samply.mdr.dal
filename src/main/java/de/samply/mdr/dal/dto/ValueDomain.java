/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

/** A value domain. */
public abstract class ValueDomain extends Element {

  private static final long serialVersionUID = 1800481463767754641L;
  /**
   * The datatype of this value domain. Keep in mind, that this field exists for compatibility
   * reasons and must not be used to validate data.
   */
  private String datatype;
  /**
   * The format of this value domain. Keep in mind, that this field exists for compatibility reasons
   * and must not be used to validate data.
   */
  private String format;
  /** The maximum length of the value. */
  private Integer maxCharacters;
  /** The unit of measure as a string, e.g. "a". */
  private String unitOfMeasure;

  public ValueDomain() {
    super();
  }

  /**
   * Create an ValueDomain dto of samply.mdr.dal from an Element dto of jooq.
   */
  public ValueDomain(de.samply.mdr.dal.jooq.tables.pojos.Element element) {
    super(element);
    setFormat(element.getFormat());
    setDatatype(element.getDatatype());
    setMaxCharacters(element.getMaximumcharacters());
    setUnitOfMeasure(element.getUnitofmeasure());
  }

  public String getDatatype() {
    return datatype;
  }

  public void setDatatype(String datatype) {
    this.datatype = datatype;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public Integer getMaxCharacters() {
    return maxCharacters;
  }

  public void setMaxCharacters(Integer maxCharacters) {
    this.maxCharacters = maxCharacters;
  }

  public String getUnitOfMeasure() {
    return unitOfMeasure;
  }

  public void setUnitOfMeasure(String unitOfMeasure) {
    this.unitOfMeasure = unitOfMeasure;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ValueDomain)) {
      return false;
    }
    ValueDomain d = (ValueDomain) obj;
    return super.equals(obj)
        && d.datatype.equals(datatype)
        && d.format.equals(format)
        && d.maxCharacters == maxCharacters
        && d.unitOfMeasure.equals(unitOfMeasure);
  }

  /**
   * Converts a ValueDomain dto of samply.mdr.dal to an Element dto of jooq.
   */
  @Override
  public de.samply.mdr.dal.jooq.tables.pojos.Element toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.Element element = super.toJooq();
    element.setDatatype(getDatatype());
    element.setFormat(getFormat());
    element.setMaximumcharacters(getMaxCharacters());
    element.setUnitofmeasure(getUnitOfMeasure());
    return element;
  }
}
