/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import java.io.Serializable;

/** A concept association for an element. */
public class ConceptAssociation implements Serializable {

  private static final long serialVersionUID = -218608973358407005L;
  private Integer id;
  private String system;
  private String source;
  private String version;
  private String term;
  private String text;
  private String linktype;
  private Integer scopedIdentifierId;

  public ConceptAssociation() {
  }

  /**
   * Create a ConceptAssociation dto of samply.mdr.dal from ConceptAssociation dto of jooq.
   */
  public ConceptAssociation(de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation ca) {
    setId(ca.getId());
    setLinktype(ca.getLinktype());
    setScopedIdentifierId(ca.getScopedidentifierId());
    setSource(ca.getSource());
    setSystem(ca.getSystem());
    setTerm(ca.getTerm());
    setText(ca.getText());
    setVersion(ca.getVersion());
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getSystem() {
    return system;
  }

  public void setSystem(String system) {
    this.system = system;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getTerm() {
    return term;
  }

  public void setTerm(String term) {
    this.term = term;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getLinktype() {
    return linktype;
  }

  public void setLinktype(String linktype) {
    this.linktype = linktype;
  }

  public Integer getScopedIdentifierId() {
    return scopedIdentifierId;
  }

  public void setScopedIdentifierId(Integer scopedIdentifierId) {
    this.scopedIdentifierId = scopedIdentifierId;
  }

  /**
   * Convert a ConceptAssociation dto of samply.mdr.dal to a ConceptAssociation to of jooq.
   */
  public de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation toJooq() {
    de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation ca =
        new de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation();

    ca.setId(getId());
    ca.setLinktype(getLinktype());
    ca.setScopedidentifierId(getScopedIdentifierId());
    ca.setSource(getSource());
    ca.setSystem(getSystem());
    ca.setTerm(getTerm());
    ca.setText(getText());
    ca.setVersion(getVersion());

    return ca;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ConceptAssociation)) {
      return false;
    }
    ConceptAssociation d = (ConceptAssociation) obj;
    return d.id == id
        && d.system.equals(system)
        && d.source.equals(source)
        && d.version.equals(version)
        && d.term.equals(term)
        && d.text.equals(text)
        && d.linktype.equals(linktype)
        && d.scopedIdentifierId == scopedIdentifierId;
  }
}
