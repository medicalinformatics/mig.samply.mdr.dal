/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.tables.pojos.Permissiblecode;
import java.io.Serializable;

/** A permissible code in a catalog value domain. */
public class PermissibleCode implements Serializable {

  private static final long serialVersionUID = -8612083040880054991L;
  /** The catalog value domains ID. */
  private Integer catalogValueDomainId;
  /** The codes scoped identifier ID. */
  private Integer codeScopedIdentifierId;

  public PermissibleCode() {
  }

  /**
   * Create a PermissibleCode dto of samply.mdr.dal from Permissiblecode dto of jooq.
   */
  public PermissibleCode(Permissiblecode permissiblecode) {
    setCatalogValueDomainId(permissiblecode.getCatalogvaluedomainid());
    setCodeScopedIdentifierId(permissiblecode.getCodescopedidentifierid());
  }

  public Integer getCatalogValueDomainId() {
    return catalogValueDomainId;
  }

  public void setCatalogValueDomainId(Integer catalogValueDomainId) {
    this.catalogValueDomainId = catalogValueDomainId;
  }

  public Integer getCodeScopedIdentifierId() {
    return codeScopedIdentifierId;
  }

  public void setCodeScopedIdentifierId(Integer codeScopedIdentifierId) {
    this.codeScopedIdentifierId = codeScopedIdentifierId;
  }

  /**
   * Convert a PermissibleCode dto of samply.mdr.dal to a Permissiblecode to of jooq.
   */
  public Permissiblecode toJooq() {
    Permissiblecode permissiblecode = new Permissiblecode();
    permissiblecode.setCatalogvaluedomainid(getCatalogValueDomainId());
    permissiblecode.setCodescopedidentifierid(getCodeScopedIdentifierId());
    return permissiblecode;
  }
}
