/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dto;

import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dal.jooq.tables.pojos.Scopedidentifier;
import java.io.Serializable;
import java.util.UUID;

/**
 * A scoped identifier gives an identified item a unique identifier plus version codes and other
 * attributes.
 *
 * @author paul
 */
public class ScopedIdentifier implements Serializable {

  private static final long serialVersionUID = 1750623556646093368L;
  /** The ScopedIdentifiers ID. */
  private Integer id;
  /** The version as a string (e.g. "1.3"). */
  private String version;
  /** The identifier, e.g. "3452431a". */
  private String identifier;
  /** Not used at this moment. */
  private String url;

  private Elementtype elementType;
  /** The real namespace name. */
  private String namespace;
  /** The users ID who created this scoped identifier. */
  private Integer createdBy;
  /** The scoped identifiers "uuid". */
  private UUID uuid;
  /** The namespace in which this scoped identifier belongs. */
  private Integer namespaceId;
  /** The identified item ID. */
  private Integer elementId;
  /** Indicates the status of this scoped identifier. */
  private Status status;

  public ScopedIdentifier() {
    setUuid(UUID.randomUUID());
  }

  /**
   * Create an ScopedIdentifier dto of samply.mdr.dal from an Scopedidentifier dto of jooq.
   */
  public ScopedIdentifier(Scopedidentifier scopedidentifier, String namespace) {
    setId(scopedidentifier.getId());
    setUuid(scopedidentifier.getUuid());
    setCreatedBy(scopedidentifier.getCreatedby());
    setElementId(scopedidentifier.getElementid());
    setElementType(scopedidentifier.getElementtype());
    setIdentifier(scopedidentifier.getIdentifier());
    setNamespaceId(scopedidentifier.getNamespaceid());
    setStatus(scopedidentifier.getStatus());
    setUrl(scopedidentifier.getUrl());
    setVersion(scopedidentifier.getVersion());
    setNamespace(namespace);
  }

  /**
   * TODO: add javadoc.
   */
  public static ScopedIdentifier fromUrn(String urn) {
    ScopedIdentifier identifier = new ScopedIdentifier();

    if (!isUrn(urn)) {
      throw new IllegalArgumentException();
    }

    String[] parts = urn.split(":");

    identifier.namespace = parts[1];
    identifier.setElementType(Elementtype.valueOf(parts[2].toUpperCase()));
    identifier.setIdentifier(parts[3]);
    identifier.setVersion(parts[4]);
    return identifier;
  }

  /**
   * TODO: add javadoc.
   */
  public static boolean isUrn(String urn) {
    if (urn == null) {
      return false;
    }

    String[] parts = urn.split(":");

    return parts.length == 5 && parts[0].equals("urn");
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getNamespaceId() {
    return namespaceId;
  }

  public void setNamespaceId(Integer namespaceId) {
    this.namespaceId = namespaceId;
  }

  public Integer getElementId() {
    return elementId;
  }

  public void setElementId(Integer identifiedId) {
    this.elementId = identifiedId;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ScopedIdentifier)) {
      return false;
    }
    ScopedIdentifier i = (ScopedIdentifier) obj;
    return i.id == id
        && i.elementId == elementId
        && i.status == status
        && i.identifier.equals(identifier)
        && i.namespaceId == namespaceId
        && i.url.equals(url)
        && i.version.equals(version)
        && elementType == i.elementType;
  }

  public boolean isFirstDraft() {
    return status == Status.DRAFT && "1".equals(version);
  }

  @Override
  public String toString() {
    return "urn:"
        + namespace
        + ":"
        + elementType.toString().toLowerCase()
        + ":"
        + identifier
        + ":"
        + version;
  }

  public Elementtype getElementType() {
    return elementType;
  }

  public void setElementType(Elementtype elementType) {
    this.elementType = elementType;
  }

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public Integer getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Integer createdBy) {
    this.createdBy = createdBy;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  /**
   * Converts a ScopedIdentifier dto of samply.mdr.dal to an Scopedidentifier dto of jooq.
   */
  public Scopedidentifier toJooq() {
    Scopedidentifier scopedidentifier = new Scopedidentifier();
    scopedidentifier.setId(getId());
    scopedidentifier.setCreatedby(getCreatedBy());
    scopedidentifier.setElementid(getElementId());
    scopedidentifier.setElementtype(getElementType());
    scopedidentifier.setIdentifier(getIdentifier());
    scopedidentifier.setNamespaceid(getNamespaceId());
    scopedidentifier.setStatus(getStatus());
    scopedidentifier.setUrl(getUrl());
    scopedidentifier.setUuid(getUuid());
    scopedidentifier.setVersion(getVersion());
    return scopedidentifier;
  }
}
