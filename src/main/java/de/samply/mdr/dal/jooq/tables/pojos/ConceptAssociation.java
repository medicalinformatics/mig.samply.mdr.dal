/*
 * This file is generated by jOOQ.
 */
package de.samply.mdr.dal.jooq.tables.pojos;


import java.io.Serializable;
import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ConceptAssociation implements Serializable {

    private static final long serialVersionUID = 81245200;

    private Integer id;
    private String system;
    private String source;
    private String version;
    private String term;
    private String text;
    private String linktype;
    private Integer scopedidentifierId;

    public ConceptAssociation() {}

    public ConceptAssociation(ConceptAssociation value) {
        this.id = value.id;
        this.system = value.system;
        this.source = value.source;
        this.version = value.version;
        this.term = value.term;
        this.text = value.text;
        this.linktype = value.linktype;
        this.scopedidentifierId = value.scopedidentifierId;
    }

    public ConceptAssociation(
        Integer id,
        String system,
        String source,
        String version,
        String term,
        String text,
        String linktype,
        Integer scopedidentifierId
    ) {
        this.id = id;
        this.system = system;
        this.source = source;
        this.version = version;
        this.term = term;
        this.text = text;
        this.linktype = linktype;
        this.scopedidentifierId = scopedidentifierId;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSystem() {
        return this.system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTerm() {
        return this.term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLinktype() {
        return this.linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public Integer getScopedidentifierId() {
        return this.scopedidentifierId;
    }

    public void setScopedidentifierId(Integer scopedidentifierId) {
        this.scopedidentifierId = scopedidentifierId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ConceptAssociation (");

        sb.append(id);
        sb.append(", ").append(system);
        sb.append(", ").append(source);
        sb.append(", ").append(version);
        sb.append(", ").append(term);
        sb.append(", ").append(text);
        sb.append(", ").append(linktype);
        sb.append(", ").append(scopedidentifierId);

        sb.append(")");
        return sb.toString();
    }
}
