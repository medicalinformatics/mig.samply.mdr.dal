/*
 * This file is generated by jOOQ.
 */
package de.samply.mdr.dal.jooq.tables;


import de.samply.mdr.dal.jooq.Indexes;
import de.samply.mdr.dal.jooq.Keys;
import de.samply.mdr.dal.jooq.Public;
import de.samply.mdr.dal.jooq.tables.records.MdruserRecord;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.JSON;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row3;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Mdruser extends TableImpl<MdruserRecord> {

    private static final long serialVersionUID = -679283173;

    /**
     * The reference instance of <code>public.mdrUser</code>
     */
    public static final Mdruser MDRUSER = new Mdruser();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<MdruserRecord> getRecordType() {
        return MdruserRecord.class;
    }

    /**
     * The column <code>public.mdrUser.id</code>.
     */
    public final TableField<MdruserRecord, Integer> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('\"mdrUser_id_seq\"'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.mdrUser.username</code>.
     */
    public final TableField<MdruserRecord, String> USERNAME = createField(DSL.name("username"), org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>public.mdrUser.data</code>.
     */
    public final TableField<MdruserRecord, JSON> DATA = createField(DSL.name("data"), org.jooq.impl.SQLDataType.JSON.nullable(false), this, "");

    /**
     * Create a <code>public.mdrUser</code> table reference
     */
    public Mdruser() {
        this(DSL.name("mdrUser"), null);
    }

    /**
     * Create an aliased <code>public.mdrUser</code> table reference
     */
    public Mdruser(String alias) {
        this(DSL.name(alias), MDRUSER);
    }

    /**
     * Create an aliased <code>public.mdrUser</code> table reference
     */
    public Mdruser(Name alias) {
        this(alias, MDRUSER);
    }

    private Mdruser(Name alias, Table<MdruserRecord> aliased) {
        this(alias, aliased, null);
    }

    private Mdruser(Name alias, Table<MdruserRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Mdruser(Table<O> child, ForeignKey<O, MdruserRecord> key) {
        super(child, key, MDRUSER);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.MDRUSER_PKEY, Indexes.MDRUSER_USERNAME_KEY);
    }

    @Override
    public Identity<MdruserRecord, Integer> getIdentity() {
        return Keys.IDENTITY_MDRUSER;
    }

    @Override
    public UniqueKey<MdruserRecord> getPrimaryKey() {
        return Keys.MDRUSER_PKEY;
    }

    @Override
    public List<UniqueKey<MdruserRecord>> getKeys() {
        return Arrays.<UniqueKey<MdruserRecord>>asList(Keys.MDRUSER_PKEY, Keys.MDRUSER_USERNAME_KEY);
    }

    @Override
    public Mdruser as(String alias) {
        return new Mdruser(DSL.name(alias), this);
    }

    @Override
    public Mdruser as(Name alias) {
        return new Mdruser(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Mdruser rename(String name) {
        return new Mdruser(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Mdruser rename(Name name) {
        return new Mdruser(name, null);
    }

    // -------------------------------------------------------------------------
    // Row3 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row3<Integer, String, JSON> fieldsRow() {
        return (Row3) super.fieldsRow();
    }
}
