/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.USER_NAMESPACE_GRANTS;
import static de.samply.mdr.dal.jooq.enums.Elementtype.NAMESPACE;
import static de.samply.mdr.dal.jooq.enums.Elementtype.PERMISSIBLE_VALUE;

import de.samply.mdr.dal.dto.Catalog;
import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.Record;
import de.samply.mdr.dal.jooq.enums.GrantType;
import de.samply.mdr.dal.jooq.tables.records.ElementRecord;
import de.samply.mdr.dal.jooq.tables.records.UserNamespaceGrantsRecord;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.jooq.DSLContext;

/** The DAO that can be used to get all sorts of elements and especially namespaces. */
public class ElementDao {

  /**
   * Saves the given element.
   */
  public static void saveElement(DSLContext ctx, int userId, Element element) {
    if (element.getUuid() == null) {
      element.setUuid(UUID.randomUUID());
    }
    int id =
        ctx.insertInto(ELEMENT)
            .set(ctx.newRecord(ELEMENT, element.toJooq()))
            .returning(ELEMENT.ID)
            .fetchOne().getId();
    element.setId(id);

    if (element instanceof Namespace) {
      Namespace ns = (Namespace) element;
      saveUserNamespaceGrant(ctx, userId, ns.getId(), GrantType.READ);
      saveUserNamespaceGrant(ctx, userId, ns.getId(), GrantType.WRITE);
      saveUserNamespaceGrant(ctx, userId, ns.getId(), GrantType.ADMIN);
    }
  }

  private static void saveUserNamespaceGrant(
      DSLContext ctx, int userId, int namespaceId, GrantType grantType) {
    UserNamespaceGrantsRecord record = ctx.newRecord(USER_NAMESPACE_GRANTS);
    record.set(USER_NAMESPACE_GRANTS.NAMESPACE_ID, namespaceId);
    record.set(USER_NAMESPACE_GRANTS.USER_ID, userId);
    record.set(USER_NAMESPACE_GRANTS.GRANT_TYPE, grantType);
    ctx.executeInsert(record);
  }

  /**
   * Returns a list of *all* namespaces.
   */
  public static List<Element> getNamespaces(DSLContext ctx) {
    return ctx.fetch(ELEMENT, ELEMENT.ELEMENTTYPE.eq(NAMESPACE))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Element.class).stream().map(Namespace::new)
        .collect(Collectors.toList());
  }

  /**
   * Returns the namespace with the given name.
   */
  public static Namespace getNamespace(DSLContext ctx, String name) {
    ElementRecord element = ctx.fetchOne(ELEMENT,
        ELEMENT.ELEMENTTYPE.eq(NAMESPACE).and(ELEMENT.NAME.eq(name)));
    return element == null
        ? null
        : new Namespace(element.into(de.samply.mdr.dal.jooq.tables.pojos.Element.class));
  }

  /**
   * Returns the element with the given id.
   */
  public static Element getElement(DSLContext ctx, int id) {
    ElementRecord element = ctx.fetchOne(ELEMENT, ELEMENT.ID.eq(id));
    return element == null
        ? null
        : getObject(element.into(de.samply.mdr.dal.jooq.tables.pojos.Element.class));
  }

  /**
   * Returns the element with the given uuid.
   */
  public static Element getElement(DSLContext ctx, UUID uuid) {
    ElementRecord element = ctx.fetchOne(ELEMENT, ELEMENT.UUID.eq(uuid));
    return element == null
        ? null
        : getObject(element.into(de.samply.mdr.dal.jooq.tables.pojos.Element.class));
  }

  /**
   * Returns all permissible values for the given value domain ID.
   */
  public static List<Element> getPermissibleValues(DSLContext ctx, int id) {
    return ctx.selectFrom(ELEMENT)
        .where(ELEMENT.ELEMENTID.eq(id))
        .and(ELEMENT.ELEMENTTYPE.eq(PERMISSIBLE_VALUE))
        .orderBy(ELEMENT.ELEMENTID)
        .fetchInto(de.samply.mdr.dal.jooq.tables.pojos.Element.class).stream()
        .map(ElementDao::getObject).collect(Collectors.toList());
  }

  /**
   * Convert an Element dto of jooq to the proper subclass of Element dto from samply.mdr.dal.
   */
  public static Element getObject(de.samply.mdr.dal.jooq.tables.pojos.Element element) {
    switch (element.getElementtype()) {
      case DATAELEMENT:
        return new DataElement(element);
      case DATAELEMENTGROUP:
        return new DataElementGroup(element);
      case RECORD:
        return new Record(element);
      case NAMESPACE:
        return new Namespace(element);
      case CATALOG:
        return new Catalog(element);
      case CODE:
        return new Code(element);
      case PERMISSIBLE_VALUE:
        return new PermissibleValue(element);
      case CATALOG_VALUE_DOMAIN:
        return new CatalogValueDomain(element);
      case DESCRIBED_VALUE_DOMAIN:
        return new DescribedValueDomain(element);
      case ENUMERATED_VALUE_DOMAIN:
        return new EnumeratedValueDomain(element);
      default:
        throw new UnsupportedOperationException();
    }
  }

}
