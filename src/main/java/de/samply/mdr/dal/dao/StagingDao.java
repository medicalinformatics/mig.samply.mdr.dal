/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.STAGING;

import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.tables.records.StagingRecord;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;

/** The DAO that can be used to get staged elements. */
public class StagingDao {
  // TODO: maybe it is also possible to directly use into(Staging) on mdr-dal dtos so we wouldn't
  //       need the stream with map.

  /**
   * Returns all elements from staging area.
   */
  public static List<Staging> getStagedElements(DSLContext ctx) {
    return ctx.fetch(STAGING, STAGING.ELEMENT_TYPE.eq(Elementtype.DATAELEMENT))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Staging.class).stream().map(Staging::new)
        .collect(Collectors.toList());
  }

  /**
   * Returns all members of a group or record from staging area.
   *
   * @param parentId id of the group or record
   */
  public static List<Staging> getStagedElementsByParentId(DSLContext ctx, int parentId) {
    return ctx.fetch(STAGING, STAGING.PARENT_ID.eq(parentId))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Staging.class).stream().map(Staging::new)
        .collect(Collectors.toList());
  }

  /**
   * Returns all root members of an import from staging area.
   *
   * @param importId id of the import
   */
  public static List<Staging> getStagedRootElementsByImportId(DSLContext ctx, int importId) {
    return ctx.fetch(STAGING, STAGING.IMPORT_ID.eq(importId), STAGING.PARENT_ID.isNull())
        .into(de.samply.mdr.dal.jooq.tables.pojos.Staging.class).stream().map(Staging::new)
        .collect(Collectors.toList());
  }

  /**
   * Returns ALL members of an import from staging area.
   *
   * @param importId id of the import
   */
  public static List<Staging> getStagedElementsByImportId(DSLContext ctx, int importId) {
    return ctx.fetch(STAGING, STAGING.IMPORT_ID.eq(importId))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Staging.class).stream().map(Staging::new)
        .collect(Collectors.toList());
  }

  /**
   * Returns the staged element with the given id.
   */
  public static Staging getStagedElementById(DSLContext ctx, int id) {
    return new Staging(ctx.fetchOne(STAGING, STAGING.ID.eq(id))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Staging.class));
  }

  /**
   * Returns a list of staged element with the given ids.
   */
  public static List<Staging> getStagedElementsById(DSLContext ctx, Collection<Integer> ids) {
    return ctx.fetch(STAGING, STAGING.ID.in(ids))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Staging.class).stream().map(Staging::new)
        .collect(Collectors.toList());
  }

  /**
   * Returns all elements of a certain element type from staging area.
   */
  public static List<Staging> getStagedElementsByType(DSLContext ctx, Elementtype type) {
    return ctx.fetch(STAGING, STAGING.ELEMENT_TYPE.eq(type))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Staging.class).stream().map(Staging::new)
        .collect(Collectors.toList());
  }

  /**
   * Saves the given list of staged elements.
   *
   * @return list of database id of the newly inserted staged elements
   */
  public static Map<Integer, Integer> saveStagedElements(
      DSLContext ctx, List<Staging> stagedElements, Map<Integer, Integer> idMap) {
    if (idMap == null) {
      idMap = new HashMap<>();
    }

    for (Staging stagedElement : stagedElements) {
      if (stagedElement.getParentId() >= 0) {
        stagedElement.setParentId(idMap.get(stagedElement.getParentId()));
      }
      int dbId = saveStagedElement(ctx, stagedElement);
      idMap.put(stagedElement.getId(), dbId);
    }

    return idMap;
  }

  /**
   * Saves the given staged element.
   *
   * @return database id of the newly inserted staged element
   */
  public static int saveStagedElement(DSLContext ctx, Staging stagedElement) {
    // TODO: the below code should be favored instead of the currently used, however I don't
    //       know if there are any problems regarding parent_id being 0 or null.
    //try (DSLContext ctx = ResourceManager.getDslContext()) {
    //  StagingRecord record = ctx.newRecord(STAGING, stagedElement);
    //  ctx.executeInsert(record);
    //}
    int pid = stagedElement.getParentId();
    return ctx.insertInto(STAGING)
        .set(STAGING.IMPORT_ID, stagedElement.getImportId())
        .set(STAGING.ELEMENT_TYPE, stagedElement.getElementType())
        .set(STAGING.DATA, stagedElement.getData())
        .set(STAGING.DESIGNATION, stagedElement.getDesignation())
        .set(STAGING.PARENT_ID, pid >= 0 ? pid : null)
        .returning(STAGING.ID)
        .fetchOne().getId();
  }

  /**
   * Deletes the given staged element.
   */
  public static void deleteStagedElement(DSLContext ctx, Staging stagedElement) {
    ctx.newRecord(STAGING, stagedElement).delete();
  }

  /**
   * Deletes the staged element with the given id.
   */
  public static void deleteStagedElementById(DSLContext ctx, int id) {
    ctx.fetchOne(STAGING, STAGING.ID.eq(id)).delete();
  }

  /**
   * Deletes the collection of staged element with the given ids.
   *
   * @param ids a list of ids to delete
   */
  public static void deleteStagedElementsById(DSLContext ctx, Collection<Integer> ids) {
    ctx.fetch(STAGING, STAGING.ID.in(ids)).forEach(StagingRecord::delete);
  }

  /**
   * Set the element UUID for a staged element when the element was successfully imported.
   *
   * @param id id of the staged element to update
   * @param elementId id of the element to link with
   */
  public static void setElementId(DSLContext ctx, int id, int elementId) {
    StagingRecord record = ctx.fetchOne(STAGING, STAGING.ID.eq(id));
    record.setElementId(elementId);
    record.store();
  }

}
