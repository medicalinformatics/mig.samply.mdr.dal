/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.DEFINITION;
import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.HIERARCHY;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIER;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIERHIERARCHY;

import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.GrantType;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dal.jooq.enums.Validationtype;
import de.samply.mdr.dal.jooq.tables.Element;
import de.samply.mdr.dal.jooq.tables.Scopedidentifierhierarchy;
import de.samply.mdr.dal.jooq.tables.pojos.Scopedidentifier;
import de.samply.mdr.dal.jooq.tables.records.ScopedidentifierRecord;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.impl.DSL;

/** DAO for scoped identifier. */
public class ScopedIdentifierDao {

  /**
   * Saves the given scoped identifier in the database.
   */
  public static int saveScopedIdentifier(DSLContext ctx, ScopedIdentifier identifier) {
    ScopedidentifierRecord record = ctx.newRecord(SCOPEDIDENTIFIER, identifier.toJooq());
    return ctx.insertInto(SCOPEDIDENTIFIER)
        .set(record)
        .returning(SCOPEDIDENTIFIER.ID)
        .fetchOne().getId();
  }

  private static ScopedIdentifier recordToScopedIdentifier(Record record) {
    Scopedidentifier si = record.into(SCOPEDIDENTIFIER.fields()).into(Scopedidentifier.class);
    String namespace = record.get(ELEMENT.as("ns").NAME);
    return new ScopedIdentifier(si, namespace);
  }

  /**
   * Returns the scoped identifier with the given ID.
   */
  public static ScopedIdentifier getScopedIdentifier(DSLContext ctx, int id) {
    Element ns = ELEMENT.as("ns");
    return ctx.select()
            .from(SCOPEDIDENTIFIER)
            .innerJoin(ns)
            .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
            .where(SCOPEDIDENTIFIER.ID.eq(id))
            .fetchOne(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Returns the scoped identifier with the given UUID in the given namespace.
   */
  public static ScopedIdentifier getScopedIdentifier(DSLContext ctx, UUID uuid) {
    Element ns = ELEMENT.as("ns");
    return ctx.select()
            .from(SCOPEDIDENTIFIER)
            .innerJoin(ns)
            .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
            .where(SCOPEDIDENTIFIER.UUID.eq(uuid))
            .fetchOne(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Returns the scoped identifier with the given UUID in the given namespace.
   * TODO: is this method even necessary? Shouldn't a UUID be unique?
   */
  public static ScopedIdentifier getScopedIdentifier(DSLContext ctx, UUID uuid, String namespace) {
    Element ns = ELEMENT.as("ns");
    return ctx.select()
            .from(SCOPEDIDENTIFIER)
            .innerJoin(ns)
            .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
            .where(SCOPEDIDENTIFIER.UUID.eq(uuid))
            .and(ns.NAME.eq(namespace))
            .fetchOne(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Returns the specified scoped identifier.
   */
  public static ScopedIdentifier getScopedIdentifier(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    return ctx.select()
            .from(SCOPEDIDENTIFIER)
            .innerJoin(ns)
            .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
            .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
            .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))
            .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
            .and(ns.NAME.eq(identifier.getNamespace()))
            .and(DaoUtil.accessibleByUserId(ctx, userId, ns))
            .fetchOne(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * TODO: add javadoc.
   */
  public static List<ScopedIdentifier> getScopedIdentifierForElement(
      DSLContext ctx, UUID elementUuid, String namespace) {
    Element ns = ELEMENT.as("ns");
    Element element = ELEMENT.as("element");
    return ctx.select()
        .from(SCOPEDIDENTIFIER)
        .innerJoin(ns)
        .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
        .leftJoin(element)
        .on(element.ID.eq(SCOPEDIDENTIFIER.ELEMENTID))
        .where(ns.UUID.eq(elementUuid))
        .and(ns.NAME.eq(namespace))
        .fetch(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Returns the scoped identifier with the specified URN.
   */
  public static ScopedIdentifier getScopedIdentifierForElement(
      DSLContext ctx, String urn, String namespace, int userId) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    return ctx.select()
        .from(SCOPEDIDENTIFIER)
        .innerJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .and(SCOPEDIDENTIFIER.ELEMENTID.eq(
            ctx.select(SCOPEDIDENTIFIER.ELEMENTID)
                .from(SCOPEDIDENTIFIER)
                .innerJoin(ns)
                .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
                .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
                .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
                .and(ns.NAME.eq(identifier.getNamespace()))))
        .and(ns.NAME.eq(namespace))
        .and(SCOPEDIDENTIFIER.STATUS.eq(Status.RELEASED))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ns))
        .fetchOne(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * TODO: add javadoc.
   */
  public static List<ScopedIdentifier> getScopedIdentifierForElementById(
      DSLContext ctx, int elementId, String namespace) {
    Element ns = ELEMENT.as("ns");
    return ctx.select()
        .from(SCOPEDIDENTIFIER)
        .innerJoin(ns)
        .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
        .where(SCOPEDIDENTIFIER.ELEMENTID.eq(elementId))
        .and(ns.NAME.eq(namespace))
        .fetch(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Returns a free identifier for the given namespace and element type.
   */
  public static String getFreeIdentifier(DSLContext ctx, String namespace, Elementtype type) {
    Element ns = ELEMENT.as("ns");
    Integer max = ctx.select(DSL.max(
        DSL.when(SCOPEDIDENTIFIER.IDENTIFIER.likeRegex("^\\d+$"),
            SCOPEDIDENTIFIER.IDENTIFIER.cast(Integer.class))
            .else_(0)))
        .from(SCOPEDIDENTIFIER)
        .leftJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .where(ns.NAME.eq(namespace))
        .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(type))
        .fetchOne().value1();
    if (max != null) {
      return String.valueOf(max + 1);
    } else {
      return "1";
    }
  }

  /**
   * Returns a new revision for the given URN, meaning the new revision is not used yet.
   */
  public static int getNewRevision(DSLContext ctx, String urn) {
    ScopedIdentifier scoped = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    Integer max = ctx.select(DSL.max(
        DSL.when(SCOPEDIDENTIFIER.VERSION.likeRegex("^\\d+$"),
            SCOPEDIDENTIFIER.VERSION.cast(Integer.class))
            .else_(0)))
        .from(SCOPEDIDENTIFIER)
        .leftJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(scoped.getIdentifier()))
        .and(ns.NAME.eq(scoped.getNamespace()))
        .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(scoped.getElementType()))
        .fetchOne().value1();
    return max + 1;
  }

  /**
   * Returns the scoped identifier in a specific namespace, that is identical to the scoped
   * identifier with the given URN.
   */
  public static ScopedIdentifier getIdenticalScopedIdentifier(
      DSLContext ctx, String urn, String namespace, int userId) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    return ctx.select()
        .from(SCOPEDIDENTIFIER)
        .innerJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .and(SCOPEDIDENTIFIER.UUID.eq(
            ctx.select(SCOPEDIDENTIFIER.UUID)
                .from(SCOPEDIDENTIFIER)
                .innerJoin(ns)
                .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
                .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
                .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
                .and(ns.NAME.eq(identifier.getNamespace()))))
        .and(ns.NAME.eq(namespace))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ns))
        .fetchOne(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Returns the latest released Scoped Identifier for the specified URN.
   */
  public static ScopedIdentifier getLatestVersion(DSLContext ctx, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    return ctx.select()
        .from(SCOPEDIDENTIFIER)
        .innerJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
        .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
        .and(ns.NAME.eq(identifier.getNamespace()))
        .and(SCOPEDIDENTIFIER.VERSION.eq(
            ctx.select(DSL.max(SCOPEDIDENTIFIER.VERSION))
                .from(SCOPEDIDENTIFIER)
                .innerJoin(ns)
                .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
                .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
                .and(ns.NAME.eq(identifier.getNamespace()))
                .and(SCOPEDIDENTIFIER.STATUS.eq(Status.RELEASED))))
        .fetchOne(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Returns a list of all available versions for the given URN.
   */
  public static List<ScopedIdentifier> getVersions(DSLContext ctx, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    return ctx.select()
        .from(SCOPEDIDENTIFIER)
        .innerJoin(ns)
        .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
        .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
        .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
        .and(ns.NAME.eq(identifier.getNamespace()))
        .fetch(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Outdates the scoped identifier with the given URN.
   */
  public static void outdateIdentifier(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    ctx.update(SCOPEDIDENTIFIER)
        .set(SCOPEDIDENTIFIER.STATUS, Status.OUTDATED)
        .where(SCOPEDIDENTIFIER.ID.in(
            ctx.select(SCOPEDIDENTIFIER.ID)
                .from(SCOPEDIDENTIFIER)
                .leftJoin(ns)
                .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
                .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
                .and(ns.NAME.eq(identifier.getNamespace()))
                .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))))
        .execute();

    if (identifier.getElementType() == Elementtype.CATALOG) {
      List<IdentifiedElement> allSubMembers =
          IdentifiedDao.getAllSubMembers(ctx, userId, identifier.toString());
      for (IdentifiedElement element : allSubMembers) {
        outdateIdentifier(ctx, userId, element.getScoped().toString());
      }
    }
  }

  /**
   * Outdates all scoped identifier with the given URN except the version. Also outdates all codes,
   * if the URN describes a catalog.
   */
  public static void outdateIdentifiers(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    Element ns = ELEMENT.as("ns");
    ctx.update(SCOPEDIDENTIFIER)
        .set(SCOPEDIDENTIFIER.STATUS, Status.OUTDATED)
        .where(SCOPEDIDENTIFIER.STATUS.eq(Status.RELEASED))
        .and(SCOPEDIDENTIFIER.ID.in(
            ctx.select(SCOPEDIDENTIFIER.ID)
                .from(SCOPEDIDENTIFIER)
                .leftJoin(ns)
                .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
                .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
                .and(ns.NAME.eq(identifier.getNamespace()))))
        .execute();

    if (identifier.getElementType() == Elementtype.CATALOG) {
      List<IdentifiedElement> allSubMembers =
          IdentifiedDao.getAllSubMembers(ctx, userId, identifier.toString());
      for (IdentifiedElement element : allSubMembers) {
        outdateIdentifiers(ctx, userId, element.getScoped().toString());
      }
    }
  }

  /**
   * Deletes the scoped identifier with the given URN.
   */
  public static void deleteDraftIdentifier(DSLContext ctx, int userId, String urn)
      throws IllegalArgumentException {
    ScopedIdentifier identifier = getScopedIdentifier(ctx, userId, urn);

    if (identifier.getStatus() == Status.DRAFT) {
      ctx.deleteFrom(SCOPEDIDENTIFIER)
          .where(SCOPEDIDENTIFIER.ID.eq(identifier.getId()))
          .execute();
    } else {
      throw new IllegalArgumentException(
          "Identifier is not a draft. Call outdateIdentifier instead.");
    }
  }

  /**
   * Deletes ALL scoped identifiers in a given list of namespaces with the status "DRAFT". Only
   * Namespace admins are allowed to do this
   */
  public static void deleteDraftIdentifiersInNamespace(DSLContext ctx, int userId,
      Collection<Integer> namespaceIds)
      throws IllegalArgumentException {
    ctx.deleteFrom(SCOPEDIDENTIFIER)
        .where(SCOPEDIDENTIFIER.STATUS.eq(Status.DRAFT))
        .and(SCOPEDIDENTIFIER.NAMESPACEID.in(namespaceIds))
        .and(SCOPEDIDENTIFIER.NAMESPACEID.in(
            DaoUtil.getUserNamespaceGrantsQuery(ctx, userId, GrantType.ADMIN)
        ))
        .execute();
  }

  /**
   * Deletes all OWN scoped identifiers in a given list of namespaces with the status "DRAFT". This
   * leaves elements created by others untouched
   */
  public static void deleteOwnDraftIdentifiersInNamespace(DSLContext ctx, int userId,
      Collection<Integer> namespaceIds)
      throws IllegalArgumentException {
    ctx.deleteFrom(SCOPEDIDENTIFIER)
        .where(SCOPEDIDENTIFIER.STATUS.eq(Status.DRAFT))
        .and(SCOPEDIDENTIFIER.NAMESPACEID.in(namespaceIds))
        .and(SCOPEDIDENTIFIER.CREATEDBY.eq(userId))
        .and(SCOPEDIDENTIFIER.NAMESPACEID.in(
            DaoUtil.getUserNamespaceGrantsQuery(ctx, userId, GrantType.WRITE)
        ))
        .execute();
  }

  /**
   * Deletes all OWN scoped identifiers in a ALL namespaces with the status "DRAFT". This leaves
   * elements created by others untouched
   */
  public static void deleteOwnDraftIdentifiers(DSLContext ctx, int userId)
      throws IllegalArgumentException {
    ctx.deleteFrom(SCOPEDIDENTIFIER)
        .where(SCOPEDIDENTIFIER.STATUS.eq(Status.DRAFT))
        .and(SCOPEDIDENTIFIER.CREATEDBY.eq(userId))
        .execute();
  }

  /**
   * Updates the status for the given Scoped Identifier.
   */

  public static void updateStatus(DSLContext ctx, ScopedIdentifier identifier) {
    ScopedidentifierRecord record = ctx.newRecord(SCOPEDIDENTIFIER, identifier.toJooq());
    ctx.executeUpdate(record);
  }

  /**
   * Releases the Scoped identifier with the given ID.
   */
  public static void release(DSLContext ctx, int id) {
    ctx.update(SCOPEDIDENTIFIER)
        .set(SCOPEDIDENTIFIER.STATUS, Status.RELEASED)
        .where(SCOPEDIDENTIFIER.ID.eq(id))
        .execute();
  }

  /**
   * Releases the given scoped identifier and all codes, if the scoped identifier describes a
   * catalog.
   */
  public static void release(DSLContext ctx, int userId, ScopedIdentifier scopedIdentifier) {
    release(ctx, scopedIdentifier.getId());

    if (scopedIdentifier.getElementType() == Elementtype.CATALOG) {
      List<IdentifiedElement> allSubMembers =
          IdentifiedDao.getAllSubMembers(ctx, userId, scopedIdentifier.toString());
      for (IdentifiedElement element : allSubMembers) {
        release(ctx, element.getScoped().getId());
      }
    }
  }

  /**
   * Returns a list of all scoped identifiers of unreleasable elements.
   */
  public static List<ScopedIdentifier> getUnreleasableElementIdentifiers(DSLContext ctx) {
    de.samply.mdr.dal.jooq.tables.Scopedidentifier si = SCOPEDIDENTIFIER.as("si");
    Scopedidentifierhierarchy s =  SCOPEDIDENTIFIERHIERARCHY.as("s");
    Element vd = ELEMENT.as("vd");
    Element de = ELEMENT.as("de");
    Element ns = ELEMENT.as("ns");
    String members = "members";
    String superId = "superId";
    String subId = "subId";

    return ctx.withRecursive(DSL.name(members)).as(
        ctx.select(SCOPEDIDENTIFIERHIERARCHY.SUPERID, SCOPEDIDENTIFIERHIERARCHY.SUBID)
            .from(SCOPEDIDENTIFIERHIERARCHY)
            .where(SCOPEDIDENTIFIERHIERARCHY.SUBID.in(
                ctx.select(si.ID)
                    .from(si)
                    .where(si.ELEMENTID.in(
                        ctx.select(de.ID)
                            .from(vd)
                            .join(de)
                            .on(vd.ID.eq(de.ELEMENTID))
                            .where(de.ELEMENTTYPE.in(Elementtype.RECORD, Elementtype.DATAELEMENT,
                                Elementtype.DATAELEMENTGROUP))
                            .and(vd.VALIDATIONTYPE.eq(Validationtype.TBD))))))
            .union(
                ctx.select(s.SUPERID, s.SUBID)
                    .from(s)
                    .innerJoin(DSL.table(DSL.name(members)))
                    .on(DSL.field(DSL.name(members, superId)).eq(s.SUBID))))
        .selectDistinct(SCOPEDIDENTIFIER.fields())
        .select(ns.NAME)
        .from(SCOPEDIDENTIFIER)
        .join(members)
        .on(SCOPEDIDENTIFIER.ID.eq(DSL.field(DSL.name(members, superId)).cast(int.class))
            .or(SCOPEDIDENTIFIER.ID.eq(DSL.field(DSL.name(members, subId)).cast(int.class))))
        .innerJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .fetch(ScopedIdentifierDao::recordToScopedIdentifier);
  }

  /**
   * Redirects the given Scoped Identifier to another element. Please note, that all previously
   * stored definitions, slots and hierarchy objects will be deleted.
   */
  public static void updateScopedIdentifier(
      DSLContext ctx, int userId, ScopedIdentifier identifier) {
    ScopedIdentifier realIdentifier =
        ScopedIdentifierDao.getScopedIdentifier(ctx, userId, identifier.toString());

    ctx.deleteFrom(DEFINITION)
        .where(DEFINITION.SCOPEDIDENTIFIERID.eq(realIdentifier.getId())).execute();
    ctx.deleteFrom(SCOPEDIDENTIFIERHIERARCHY)
        .where(SCOPEDIDENTIFIERHIERARCHY.SUPERID.eq(realIdentifier.getId())).execute();
    SlotDao.deleteSlotByScopedIdentifierId(ctx, realIdentifier.getId());

    /* The default for a new UUID for scoped identifiers is just a random UUID. */
    identifier.setUuid(UUID.randomUUID());
    ctx.update(SCOPEDIDENTIFIER)
        .set(SCOPEDIDENTIFIER.ELEMENTID, identifier.getElementId())
        .set(SCOPEDIDENTIFIER.STATUS, identifier.getStatus())
        .set(SCOPEDIDENTIFIER.UUID, identifier.getUuid())
        .where(SCOPEDIDENTIFIER.ID.eq(realIdentifier.getId())).execute();
  }

  /** Cleanup the database. Removes old ScopedIdentifier for codes. */
  public static void cleanup(DSLContext ctx) {
    ctx.deleteFrom(SCOPEDIDENTIFIER)
        .where(SCOPEDIDENTIFIER.STATUS.eq(Status.DRAFT))
        .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(Elementtype.CODE))
        .and(SCOPEDIDENTIFIER.ID.notIn(
            ctx.select(HIERARCHY.SUB)
                .from(HIERARCHY)
        )).execute();
  }

}
