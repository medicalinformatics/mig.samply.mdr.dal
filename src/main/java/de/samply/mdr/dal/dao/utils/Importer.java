/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao.utils;

import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.PermissibleCodeDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.ScopedIdentifierHierarchyDao;
import de.samply.mdr.dal.dao.SlotDao;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.PermissibleCode;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dal.jooq.enums.Validationtype;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.CatalogValueDomain;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.DataElement;
import de.samply.mdr.xsd.DataElementGroup;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.DescribedValueDomain;
import de.samply.mdr.xsd.Element;
import de.samply.mdr.xsd.EnumeratedValueDomain;
import de.samply.mdr.xsd.Export;
import de.samply.mdr.xsd.Namespace;
import de.samply.mdr.xsd.PermissibleValue;
import de.samply.mdr.xsd.Record;
import de.samply.mdr.xsd.ScopedIdentifier;
import de.samply.mdr.xsd.Slot;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import javax.xml.bind.JAXBElement;
import org.jooq.DSLContext;

/**
 * The Importer imports a URN into another namespace. If the URN is a group or a record, all
 * subelements will be imported too.
 *
 * @author paul
 */
public class Importer {

  /** The Map that maps all already imported URNs to their new URNs. */
  private HashMap<String, String> mapper = new HashMap<>();

  private HashMap<String, ImportStrategy> strategy = new HashMap<>();

  private HashMap<String, Element> elementMap = new HashMap<>();

  private HashMap<String, Object> index = new HashMap<>();

  private HashMap<String, HashMap<String, ScopedIdentifier>> identifierMap = new HashMap<>();

  private List<Import> imports = new ArrayList<>();

  /**
   * Imports the given URN into the given namespace. Imports all subelements into the given
   * namespace as well.
   *
   * @param urn the URN that will be imported
   * @param newNamespace the new namespace
   * @param userId the current user ID
   */
  public de.samply.mdr.dal.dto.ScopedIdentifier importElement(
      DSLContext ctx, String urn, String newNamespace, int userId) {

    if (mapper.containsKey(urn)) {
      return ScopedIdentifierDao.getScopedIdentifier(ctx, userId, mapper.get(urn));
    }

    de.samply.mdr.dal.dto.ScopedIdentifier scoped =
        de.samply.mdr.dal.dto.ScopedIdentifier.fromUrn(urn);

    if (scoped.getNamespace().equals(newNamespace)) {
      return ScopedIdentifierDao.getScopedIdentifier(ctx, userId, urn);
    }

    de.samply.mdr.dal.dto.ScopedIdentifier newScoped =
        IdentifiedDao.importElement(ctx, urn, newNamespace, userId);
    mapper.put(urn, newScoped.toString());

    switch (scoped.getElementType()) {
      case DATAELEMENTGROUP:
        for (IdentifiedElement element : IdentifiedDao.getSubMembers(ctx, userId, urn)) {
          de.samply.mdr.dal.dto.ScopedIdentifier sub =
              importElement(ctx, element.getScoped().toString(), newNamespace, userId);
          ScopedIdentifierHierarchyDao.addSubIdentifier(ctx, newScoped.getId(), sub.getId());
        }
        break;

      case RECORD:
        int index = 1;
        for (IdentifiedElement element : IdentifiedDao.getEntries(ctx, userId, urn)) {
          de.samply.mdr.dal.dto.ScopedIdentifier sub =
              importElement(ctx, element.getScoped().toString(), newNamespace, userId);
          ScopedIdentifierHierarchyDao
              .addSubIdentifier(ctx, newScoped.getId(), sub.getId(), index++);
        }
        break;

      default:
        break;
    }

    return newScoped;
  }

  private Object importElement(
      DSLContext ctx, Element element, boolean keepIdentifier, int userId) {
    if (index.containsKey(element.getUuid())) {
      return index.get(element.getUuid());
    }

    /*
     * If this element is already in the database, use it. This will not work for scoped
     * identifiers, those must follow the import strategy!
     */
    de.samply.mdr.dal.dto.Element existing =
        ElementDao.getElement(ctx, UUID.fromString(element.getUuid()));

    if (existing != null) {
      index.put(element.getUuid(), existing);
    } else if (element instanceof Namespace) {
      System.out.println("Ignoring namespace!");
    } else if (element instanceof DataElement) {
      DataElement de = (DataElement) element;
      importDataElement(ctx, de, keepIdentifier, userId);
    } else if (element instanceof Catalog) {
      Catalog catalog = (Catalog) element;
      importCatalog(ctx, catalog, keepIdentifier, userId);
    } else if (element instanceof Code) {
      Code code = (Code) element;
      importCode(ctx, code, keepIdentifier, userId);
    } else if (element instanceof DataElementGroup) {
      DataElementGroup group = (DataElementGroup) element;
      importDataElementGroup(ctx, group, keepIdentifier, userId);
    } else if (element instanceof Record) {
      Record record = (Record) element;
      importRecord(ctx, record, keepIdentifier, userId);
    } else if (element instanceof EnumeratedValueDomain) {
      EnumeratedValueDomain enumerated = (EnumeratedValueDomain) element;
      importEnumeratedValueDomain(ctx, enumerated, keepIdentifier, userId);
    } else if (element instanceof DescribedValueDomain) {
      DescribedValueDomain described = (DescribedValueDomain) element;
      importDescribedValueDomain(ctx, described, keepIdentifier, userId);
    } else if (element instanceof CatalogValueDomain) {
      CatalogValueDomain catalogVd = (CatalogValueDomain) element;
      importCatalogValueDomain(ctx, catalogVd, keepIdentifier, userId);
    } else if (element instanceof PermissibleValue) {
      PermissibleValue value = (PermissibleValue) element;
      importPermissibleValue(ctx, value, keepIdentifier, userId);
    } else if (element instanceof ScopedIdentifier) {
      ScopedIdentifier identifier = (ScopedIdentifier) element;
      importScopedIdentifier(ctx, identifier, keepIdentifier, userId);
    }

    return index.get(element.getUuid());
  }

  /** Imports the given export with the given import strategy into the given database connection. */
  public void importExport(
      DSLContext ctx, Export export, List<ImportStrategy> strategy, boolean keepIdentifier,
      int userId) {
    for (ImportStrategy imp : strategy) {
      this.strategy.put(imp.from, imp);
    }

    for (JAXBElement<? extends Element> jaxbElement : export.getElement()) {
      Element element = jaxbElement.getValue();

      elementMap.put(element.getUuid(), element);

      if (element instanceof ScopedIdentifier) {
        put((ScopedIdentifier) element);
      }
    }

    for (JAXBElement<? extends Element> jaxbElement : export.getElement()) {
      Element element = jaxbElement.getValue();

      importElement(ctx, element, keepIdentifier, userId);
    }
  }

  /** Puts the given scoped identifier into the map for scoped identifiers. */
  private void put(ScopedIdentifier element) {
    if (!identifierMap.containsKey(element.getNamespace())) {
      identifierMap.put(element.getNamespace(), new HashMap<String, ScopedIdentifier>());
    }

    identifierMap.get(element.getNamespace()).put(element.getUuid(), element);
  }

  /** Gets the scoped identifier in the given namespace uuid with the given uuid. */
  private ScopedIdentifier get(String uuidNamespace, String uuidIdentifier) {
    return identifierMap.get(uuidNamespace).get(uuidIdentifier);
  }

  private void importScopedIdentifier(
      DSLContext ctx, ScopedIdentifier identifier, boolean keepIdentifier, int userId) {
    /*
     * There are two types of scoped identifiers that exist exactly once: scoped identifiers for
     * codes and catalogs. So check if the already exist in the database.
     */
    Element referencedElement = elementMap.get(identifier.getElement());
    Namespace namespace = (Namespace) elementMap.get(identifier.getNamespace());
    String targetNamespace = strategy.get(namespace.getName()).to;

    Object imported = importElement(ctx, referencedElement, keepIdentifier, userId);

    de.samply.mdr.dal.dto.ScopedIdentifier existingIdentifier = ScopedIdentifierDao
        .getScopedIdentifier(ctx, UUID.fromString(identifier.getUuid()), targetNamespace);

    /*
     * If the scoped identifier already exists in the target namespace, put it into the index map
     */
    if (existingIdentifier != null) {
      index.put(identifier.getUuid(), existingIdentifier);

      /* We have to update the status if the identifier already exists. */
      if (keepIdentifier
          && !existingIdentifier.getStatus().toString().equals(identifier.getStatus().toString())) {
        existingIdentifier.setStatus(Status.valueOf(identifier.getStatus().toString()));
        ScopedIdentifierDao.updateStatus(ctx, existingIdentifier);
      }
    } else {

      de.samply.mdr.dal.dto.Element importedElement = (de.samply.mdr.dal.dto.Element) imported;

      de.samply.mdr.dal.dto.ScopedIdentifier newIdentifier =
          new de.samply.mdr.dal.dto.ScopedIdentifier();
      newIdentifier.setCreatedBy(userId);
      newIdentifier.setElementId(importedElement.getId());
      newIdentifier.setElementType(importedElement.getElementType());
      newIdentifier.setUrl("none");
      newIdentifier.setUuid(UUID.fromString(identifier.getUuid()));
      newIdentifier.setNamespace(targetNamespace);
      newIdentifier.setStatus(Status.valueOf(identifier.getStatus().toString()));
      newIdentifier.setVersion(identifier.getVersion());

      if (keepIdentifier) {
        newIdentifier.setIdentifier(identifier.getIdentifier());
        newIdentifier.setNamespaceId(
            NamespaceDao.getNamespace(ctx, userId, targetNamespace).getElement().getId());
      } else {
        /*
         * If we don't need to keep the identifier from the export, search for free identifiers.
         *
         * <p>Try to find an identifier that references the referencedElement in the target
         * namespace
         */
        List<de.samply.mdr.dal.dto.ScopedIdentifier> foundIdentifiers =
            ScopedIdentifierDao.getScopedIdentifierForElement(
                ctx, UUID.fromString(referencedElement.getUuid()), targetNamespace);

        /* So there is at least one, find a new identifier and save it! */
        if (foundIdentifiers.size() > 0) {
          newIdentifier.setIdentifier(foundIdentifiers.get(0).getIdentifier());
          newIdentifier.setNamespaceId(foundIdentifiers.get(0).getNamespaceId());
        } else {
          newIdentifier.setIdentifier(ScopedIdentifierDao
                   .getFreeIdentifier(ctx, targetNamespace, importedElement.getElementType()));
          newIdentifier.setNamespaceId(
              NamespaceDao.getNamespace(ctx, userId, targetNamespace).getElement().getId());
        }
      }

      newIdentifier.setId(ScopedIdentifierDao.saveScopedIdentifier(ctx, newIdentifier));

      /* Save all definitions */
      for (Definition def : identifier.getDefinitions().getDefinition()) {
        de.samply.mdr.dal.dto.Element referencedImportedElement =
            (de.samply.mdr.dal.dto.Element)
                importElement(ctx, elementMap.get(def.getUuid()), keepIdentifier, userId);

        de.samply.mdr.dal.dto.Definition newDefinition = new de.samply.mdr.dal.dto.Definition();
        newDefinition.setDefinition(def.getDefinition());
        newDefinition.setDesignation(def.getDesignation());
        newDefinition.setLanguage(def.getLang());
        newDefinition.setScopedIdentifierId(newIdentifier.getId());
        newDefinition.setElementId(referencedImportedElement.getId());
        DefinitionDao.saveDefinition(ctx, newDefinition);
      }

      /* Save all slots */
      for (Slot slot : identifier.getSlots().getSlot()) {
        de.samply.mdr.dal.dto.Slot newSlot = new de.samply.mdr.dal.dto.Slot();
        newSlot.setKey(slot.getKey());
        newSlot.setValue(slot.getValue());
        newSlot.setScopedIdentifierId(newIdentifier.getId());
        SlotDao.saveSlot(ctx, newSlot);
      }

      /* Save the hierarchy */
      int order = 1;
      for (String sub : identifier.getSub()) {
        de.samply.mdr.dal.dto.ScopedIdentifier subIdentifier =
            (de.samply.mdr.dal.dto.ScopedIdentifier)
                importElement(ctx, get(identifier.getNamespace(), sub), keepIdentifier, userId);

        ScopedIdentifierHierarchyDao
            .addSubIdentifier(ctx, newIdentifier.getId(), subIdentifier.getId(), order++);
      }

      String sourceNamespace = ((Namespace) elementMap.get(identifier.getNamespace())).getName();
      Import imp = new Import();
      imp.setTo(newIdentifier.toString());
      imp.setFrom(
          "urn:"
              + sourceNamespace
              + ":"
              + newIdentifier.getElementType().toString().toLowerCase()
              + ":"
              + identifier.getIdentifier()
              + ":"
              + identifier.getVersion());
      imports.add(imp);

      index.put(identifier.getUuid(), newIdentifier);
    }
  }

  private void importPermissibleValue(
      DSLContext ctx, PermissibleValue value, boolean keepIdentifier, int userId) {
    ValueDomain valueDomain = (ValueDomain)
        importElement(ctx, elementMap.get(value.getValueDomain()), keepIdentifier, userId);

    de.samply.mdr.dal.dto.PermissibleValue newValue = new de.samply.mdr.dal.dto.PermissibleValue();
    newValue.setUuid(UUID.fromString(value.getUuid()));
    newValue.setElementType(Elementtype.PERMISSIBLE_VALUE);
    newValue.setPermittedValue(value.getValue());
    newValue.setValueDomainId(valueDomain.getId());

    ElementDao.saveElement(ctx, userId, newValue);

    index.put(value.getUuid(), newValue);
  }

  private void importCatalogValueDomain(
      DSLContext ctx, CatalogValueDomain catalogVd, boolean keepIdentifier, int userId) {
    de.samply.mdr.dal.dto.ScopedIdentifier catalogIdentifier =
        (de.samply.mdr.dal.dto.ScopedIdentifier)
            importElement(ctx, elementMap.get(catalogVd.getCatalog()), keepIdentifier, userId);

    de.samply.mdr.dal.dto.CatalogValueDomain newCatalogVd =
        new de.samply.mdr.dal.dto.CatalogValueDomain();
    newCatalogVd.setDatatype(catalogVd.getDatatype());
    newCatalogVd.setFormat(catalogVd.getFormat());
    newCatalogVd.setMaxCharacters(catalogVd.getMaxCharacters().intValue());
    newCatalogVd.setUnitOfMeasure(catalogVd.getUnitOfMeasure());
    newCatalogVd.setCatalogScopedIdentifierId(catalogIdentifier.getId());

    newCatalogVd.setElementType(Elementtype.ENUMERATED_VALUE_DOMAIN);
    newCatalogVd.setUuid(UUID.fromString(catalogVd.getUuid()));

    ElementDao.saveElement(ctx, userId, newCatalogVd);

    index.put(catalogVd.getUuid(), newCatalogVd);

    for (String permissibleCode : catalogVd.getPermissibleCode()) {
      de.samply.mdr.dal.dto.ScopedIdentifier code =
          (de.samply.mdr.dal.dto.ScopedIdentifier)
              importElement(ctx, elementMap.get(permissibleCode), keepIdentifier, userId);

      PermissibleCode newPermissibleCode = new PermissibleCode();
      newPermissibleCode.setCatalogValueDomainId(newCatalogVd.getId());
      newPermissibleCode.setCodeScopedIdentifierId(code.getId());

      PermissibleCodeDao.savePermissibleCode(ctx, newPermissibleCode);
    }
  }

  private void importDescribedValueDomain(
      DSLContext ctx, DescribedValueDomain described, boolean keepIdentifier, int userId) {
    de.samply.mdr.dal.dto.DescribedValueDomain newDescribed =
        new de.samply.mdr.dal.dto.DescribedValueDomain();
    newDescribed.setDatatype(described.getDatatype());
    newDescribed.setFormat(described.getFormat());
    newDescribed.setMaxCharacters(described.getMaxCharacters().intValue());
    newDescribed.setUnitOfMeasure(described.getUnitOfMeasure());

    newDescribed.setDescription(described.getDescription());
    newDescribed.setValidationData(described.getValidationData());
    newDescribed.setValidationType(Validationtype.valueOf(described.getValidationType()));

    newDescribed.setElementType(Elementtype.ENUMERATED_VALUE_DOMAIN);
    newDescribed.setUuid(UUID.fromString(described.getUuid()));

    ElementDao.saveElement(ctx, userId, newDescribed);

    index.put(described.getUuid(), newDescribed);
  }

  private void importEnumeratedValueDomain(
      DSLContext ctx, EnumeratedValueDomain enumerated, boolean keepIdentifier, int userId) {
    de.samply.mdr.dal.dto.EnumeratedValueDomain newEnumerated =
        new de.samply.mdr.dal.dto.EnumeratedValueDomain();
    newEnumerated.setDatatype(enumerated.getDatatype());
    newEnumerated.setFormat(enumerated.getFormat());
    newEnumerated.setMaxCharacters(enumerated.getMaxCharacters().intValue());
    newEnumerated.setUnitOfMeasure(enumerated.getUnitOfMeasure());

    newEnumerated.setElementType(Elementtype.ENUMERATED_VALUE_DOMAIN);
    newEnumerated.setUuid(UUID.fromString(enumerated.getUuid()));

    ElementDao.saveElement(ctx, userId, newEnumerated);

    index.put(enumerated.getUuid(), newEnumerated);
  }

  private void importRecord(DSLContext ctx, Record record, boolean keepIdentifier, int userId) {
    de.samply.mdr.dal.dto.Record newRecord = new de.samply.mdr.dal.dto.Record();
    newRecord.setElementType(Elementtype.RECORD);
    newRecord.setUuid(UUID.fromString(record.getUuid()));

    ElementDao.saveElement(ctx, userId, newRecord);

    index.put(record.getUuid(), newRecord);
  }

  private void importDataElementGroup(
      DSLContext ctx, DataElementGroup group, boolean keepIdentifier, int userId) {
    de.samply.mdr.dal.dto.DataElementGroup newGroup = new de.samply.mdr.dal.dto.DataElementGroup();
    newGroup.setElementType(Elementtype.DATAELEMENTGROUP);
    newGroup.setUuid(UUID.fromString(group.getUuid()));

    ElementDao.saveElement(ctx, userId, newGroup);

    index.put(group.getUuid(), newGroup);
  }

  private void importCode(DSLContext ctx, Code code, boolean keepIdentifier, int userId) {
    de.samply.mdr.dal.dto.Catalog catalog =
        (de.samply.mdr.dal.dto.Catalog)
            importElement(ctx, elementMap.get(code.getCatalog()), keepIdentifier, userId);

    de.samply.mdr.dal.dto.Code newCode = new de.samply.mdr.dal.dto.Code();
    newCode.setCatalogId(catalog.getId());
    newCode.setCode(code.getCode());
    newCode.setValid(code.isIsValid());

    newCode.setElementType(Elementtype.CODE);
    newCode.setUuid(UUID.fromString(code.getUuid()));

    ElementDao.saveElement(ctx, userId, newCode);

    index.put(code.getUuid(), newCode);
  }

  private void importCatalog(DSLContext ctx, Catalog catalog, boolean keepIdentifier, int userId) {
    de.samply.mdr.dal.dto.Catalog newCatalog = new de.samply.mdr.dal.dto.Catalog();
    newCatalog.setUuid(UUID.fromString(catalog.getUuid()));
    newCatalog.setElementType(Elementtype.CATALOG);

    ElementDao.saveElement(ctx, userId, newCatalog);

    index.put(catalog.getUuid(), newCatalog);
  }

  private void importDataElement(
      DSLContext ctx, DataElement de, boolean keepIdentifier, int userId) {
    ValueDomain domain = (ValueDomain)
        importElement(ctx, elementMap.get(de.getValueDomain()), keepIdentifier, userId);

    de.samply.mdr.dal.dto.DataElement newDataElement = new de.samply.mdr.dal.dto.DataElement();
    newDataElement.setElementType(Elementtype.DATAELEMENT);
    newDataElement.setValueDomainId(domain.getId());
    newDataElement.setUuid(UUID.fromString(de.getUuid()));

    ElementDao.saveElement(ctx, userId, newDataElement);

    index.put(de.getUuid(), newDataElement);
  }

  public List<Import> getImports() {
    return imports;
  }

  public static class ImportStrategy implements Serializable {

    private static final long serialVersionUID = 8613418983923958724L;

    private String from;

    private String to;

    public String getFrom() {
      return from;
    }

    public void setFrom(String from) {
      this.from = from;
    }

    public String getTo() {
      return to;
    }

    public void setTo(String to) {
      this.to = to;
    }
  }

  public static class Import implements Serializable {

    private static final long serialVersionUID = -3278169165551597247L;

    private String from;

    private String to;

    public String getFrom() {
      return from;
    }

    public void setFrom(String from) {
      this.from = from;
    }

    public String getTo() {
      return to;
    }

    public void setTo(String to) {
      this.to = to;
    }
  }
}
