/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao.utils;

import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.SlotDao;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.CatalogValueDomain;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.DataElement;
import de.samply.mdr.xsd.DataElementGroup;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.Definitions;
import de.samply.mdr.xsd.DescribedValueDomain;
import de.samply.mdr.xsd.EnumeratedValueDomain;
import de.samply.mdr.xsd.Export;
import de.samply.mdr.xsd.Namespace;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.mdr.xsd.PermissibleValue;
import de.samply.mdr.xsd.Record;
import de.samply.mdr.xsd.ScopedIdentifier;
import de.samply.mdr.xsd.Slot;
import de.samply.mdr.xsd.Slots;
import de.samply.mdr.xsd.Status;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import javax.xml.bind.JAXBElement;
import org.jooq.DSLContext;

/**
 * This class exports elements from an MDR and stores them as an Export element, which then can be
 * serialized into XML.
 */
public class Exporter {

  /** The list of described elements. */
  private List<DescribedElement> elements = new ArrayList<>();
  /** The map for namespaces. */
  private HashMap<String, DescribedElement> namespaceMap = new HashMap<>();

  /** The hash set of all UUIDs, that have been exported so far. */
  private HashSet<UUID> exported = new HashSet<>();

  /** The target serializable export. */
  private Export target = new Export();

  /** The object factory for new serializable objects. */
  private ObjectFactory factory = new ObjectFactory();

  /** The hash map for all catalogs. */
  private HashMap<Integer, Element> catalogMap = new HashMap<>();

  /**
   * Adds the given described element to the list of elements, that will be exported once
   * generateExport is called.
   */
  public void add(DescribedElement element) {

    if (element != null) {
      elements.add(element);
    } else {
      throw new UnsupportedOperationException("Exporting null!");
    }
  }

  /** If the given element hasn't been exported yet, export it. */
  private void add(JAXBElement<? extends de.samply.mdr.xsd.Element> element) {
    if (!exported.contains(UUID.fromString(element.getValue().getUuid()))) {
      target.getElement().add(element);
      exported.add(UUID.fromString(element.getValue().getUuid()));
    }
  }

  /** Generates the serializable Export. */
  public Export generateExport(DSLContext ctx, int userId, boolean fullExport) {
    for (DescribedElement element : elements) {
      if (element instanceof IdentifiedElement) {
        IdentifiedElement identified = (IdentifiedElement) element;
        export(ctx, identified, userId);
      } else {
        /* If the element is not identified through a scoped identifier, it is a namespace. */
        de.samply.mdr.dal.dto.Namespace namespace =
            (de.samply.mdr.dal.dto.Namespace) element.getElement();

        List<IdentifiedElement> elementList = null;

        if (fullExport) {
          elementList =
              IdentifiedDao.findElements(
                  ctx,
                  userId,
                  "",
                  namespace.getName(),
                  Elementtype.values(),
                  de.samply.mdr.dal.jooq.enums.Status.values(),
                  new HashMap<String, String>());
        } else {
          elementList = IdentifiedDao.getRootElements(ctx, userId, namespace.getName(),
              de.samply.mdr.dal.jooq.enums.Status.RELEASED);
        }

        for (IdentifiedElement identifiedElement : elementList) {
          export(ctx, identifiedElement, userId);
        }
      }
    }

    return target;
  }

  /** Exports the given identified element and all necessary. */
  private void export(DSLContext ctx, IdentifiedElement identified, int userId) {

    if (exported.contains(identified.getScoped().getUuid())) {
      return;
    }

    de.samply.mdr.dal.dto.Namespace ns =
        getNamespace(ctx, identified.getScoped().getNamespace(), userId);
    Element element = identified.getElement();

    ScopedIdentifier exportedScopedIdentifier = new ScopedIdentifier();
    exportedScopedIdentifier.setIdentifier(identified.getScoped().getIdentifier());
    exportedScopedIdentifier.setNamespace(ns.getUuid().toString());
    exportedScopedIdentifier.setUuid(identified.getScoped().getUuid().toString());
    exportedScopedIdentifier.setVersion(identified.getScoped().getVersion());
    exportedScopedIdentifier.setElement(element.getUuid().toString());
    exportedScopedIdentifier.setStatus(
        Status.valueOf(identified.getScoped().getStatus().toString()));

    exportedScopedIdentifier.setDefinitions(new Definitions());
    exportedScopedIdentifier
        .getDefinitions()
        .getDefinition()
        .addAll(convert(identified.getDefinitions(), identified.getElement().getUuid()));

    List<de.samply.mdr.dal.dto.Slot> slots =
        SlotDao.getSlots(ctx, identified.getScoped().toString());
    exportedScopedIdentifier.setSlots(new Slots());
    for (de.samply.mdr.dal.dto.Slot slot : slots) {
      Slot expSlot = new Slot();
      expSlot.setKey(slot.getKey());
      expSlot.setValue(slot.getValue());
      exportedScopedIdentifier.getSlots().getSlot().add(expSlot);
    }

    if (!exported.contains(ns.getUuid())) {
      export(ctx, ns, userId);
    }

    if (element instanceof de.samply.mdr.dal.dto.DataElementGroup) {
      exportSubMembers(ctx, exportedScopedIdentifier, identified.getScoped().toString(), userId);

      DataElementGroup expGroup = new DataElementGroup();
      expGroup.setUuid(element.getUuid().toString());
      add(factory.createDataElementGroup(expGroup));
    } else if (element instanceof de.samply.mdr.dal.dto.DataElement) {
      DataElement expEl = new DataElement();
      expEl.setUuid(element.getUuid().toString());

      Element valueDomain = ElementDao.getElement(
          ctx, ((de.samply.mdr.dal.dto.DataElement) element).getValueDomainId());

      /* The value domain is not identified, so we have to do it manually. */
      if (valueDomain instanceof de.samply.mdr.dal.dto.DescribedValueDomain) {
        de.samply.mdr.dal.dto.DescribedValueDomain descVd =
            (de.samply.mdr.dal.dto.DescribedValueDomain) valueDomain;

        DescribedValueDomain expVd = new DescribedValueDomain();
        expVd.setDatatype(descVd.getDatatype());
        expVd.setFormat(descVd.getFormat());
        expVd.setMaxCharacters(BigInteger.valueOf(descVd.getMaxCharacters()));
        expVd.setUnitOfMeasure(descVd.getUnitOfMeasure());
        expVd.setUuid(descVd.getUuid().toString());

        expVd.setDescription(descVd.getDescription());
        expVd.setValidationData(descVd.getValidationData());
        expVd.setValidationType(descVd.getValidationType().toString());

        add(factory.createDescribedValueDomain(expVd));

      } else if (valueDomain instanceof de.samply.mdr.dal.dto.EnumeratedValueDomain) {
        de.samply.mdr.dal.dto.EnumeratedValueDomain enumVd =
            (de.samply.mdr.dal.dto.EnumeratedValueDomain) valueDomain;

        EnumeratedValueDomain expVd = new EnumeratedValueDomain();
        expVd.setDatatype(enumVd.getDatatype());
        expVd.setFormat(enumVd.getFormat());
        expVd.setMaxCharacters(BigInteger.valueOf(enumVd.getMaxCharacters()));
        expVd.setUnitOfMeasure(enumVd.getUnitOfMeasure());
        expVd.setUuid(enumVd.getUuid().toString());

        List<Element> permissibleValues = ElementDao.getPermissibleValues(ctx, valueDomain.getId());

        for (Element pvElement : permissibleValues) {
          de.samply.mdr.dal.dto.PermissibleValue pv =
              (de.samply.mdr.dal.dto.PermissibleValue) pvElement;
          PermissibleValue expValue = new PermissibleValue();

          expValue.setUuid(pv.getUuid().toString());
          expValue.setValueDomain(valueDomain.getUuid().toString());
          expValue.setValue(pv.getPermittedValue());

          exportedScopedIdentifier
              .getDefinitions()
              .getDefinition()
              .addAll(convert(DefinitionDao.getDefinitions(
                  ctx, pv.getId(), identified.getScoped().getId()), pv.getUuid()));

          add(factory.createPermissibleValue(expValue));
        }

        add(factory.createEnumeratedValueDomain(expVd));
      } else if (valueDomain instanceof de.samply.mdr.dal.dto.CatalogValueDomain) {
        de.samply.mdr.dal.dto.CatalogValueDomain catVd =
            (de.samply.mdr.dal.dto.CatalogValueDomain) valueDomain;

        IdentifiedElement catalog =
            IdentifiedDao.getElement(ctx, userId, catVd.getCatalogScopedIdentifierId());

        CatalogValueDomain expVd = new CatalogValueDomain();
        expVd.setDatatype(catVd.getDatatype());
        expVd.setFormat(catVd.getFormat());
        expVd.setMaxCharacters(BigInteger.valueOf(catVd.getMaxCharacters()));
        expVd.setUnitOfMeasure(catVd.getUnitOfMeasure());
        expVd.setCatalog(catalog.getScoped().getUuid().toString());
        expVd.setUuid(catVd.getUuid().toString());

        List<IdentifiedElement> permissibleCodes =
            IdentifiedDao.getPermissibleCodes(ctx, userId, catVd);
        for (IdentifiedElement permissibleCode : permissibleCodes) {
          expVd.getPermissibleCode().add(permissibleCode.getScoped().getUuid().toString());
        }

        add(factory.createCatalogValueDomain(expVd));

        /* Don't forget to export the catalog too. */
        export(ctx, catalog, userId);
      }

      expEl.setValueDomain(valueDomain.getUuid().toString());

      add(factory.createDataElement(expEl));
    } else if (element instanceof de.samply.mdr.dal.dto.Record) {
      exportSubMembers(ctx, exportedScopedIdentifier, identified.getScoped().toString(), userId);

      Record expRecord = new Record();
      expRecord.setUuid(element.getUuid().toString());
      add(factory.createRecord(expRecord));
    } else if (element instanceof de.samply.mdr.dal.dto.Catalog) {
      exportSubMembers(ctx, exportedScopedIdentifier, identified.getScoped().toString(), userId);

      Catalog expCat = new Catalog();
      expCat.setUuid(element.getUuid().toString());
      add(factory.createCatalog(expCat));
    } else if (element instanceof de.samply.mdr.dal.dto.Code) {
      exportSubMembers(ctx, exportedScopedIdentifier, identified.getScoped().toString(), userId);

      de.samply.mdr.dal.dto.Code code = (de.samply.mdr.dal.dto.Code) element;

      Code expCode = new Code();
      expCode.setUuid(element.getUuid().toString());
      expCode.setIsValid(code.isValid());
      expCode.setCode(code.getCode());

      /*
       * The catalog is different from other elements, since it holds a reference to the catalog. In
       * this case we need to get the catalog from database, if we haven't already, and get the UUID
       * of the catalog. We need to export the UUID.
       */
      if (!catalogMap.containsKey(code.getCatalogId())) {
        catalogMap.put(
            code.getCatalogId(), ElementDao.getElement(ctx, code.getCatalogId()));
      }

      /* And assign the UUID accordingly. */
      expCode.setCatalog(catalogMap.get(code.getCatalogId()).getUuid().toString());

      add(factory.createCode(expCode));
    }

    add(factory.createScopedIdentifier(exportedScopedIdentifier));
  }

  private void export(DSLContext ctx, de.samply.mdr.dal.dto.Namespace ns, int userId) {
    Namespace expns = new Namespace();
    expns.setUuid(ns.getUuid().toString());
    expns.setName(ns.getName());

    DescribedElement descNs = NamespaceDao.getNamespace(ctx, userId, ns.getName());

    expns.setDefinitions(new Definitions());
    expns.getDefinitions().getDefinition().addAll(convert(descNs.getDefinitions(), ns.getUuid()));

    target.getElement().add(factory.createNamespace(expns));
    exported.add(ns.getUuid());
  }

  private void exportSubMembers(
      DSLContext ctx, ScopedIdentifier exportedScopedIdentifier, String s, int userId) {
    for (IdentifiedElement e : IdentifiedDao.getSubMembers(ctx, userId, s)) {
      export(ctx, e, userId);
      exportedScopedIdentifier.getSub().add(e.getScoped().getUuid().toString());
    }
  }

  /** Converts the given definition for the given UUID. */
  private Definition convert(de.samply.mdr.dal.dto.Definition def, UUID uuid) {
    Definition exp = new Definition();
    exp.setDefinition(def.getDefinition());
    exp.setDesignation(def.getDesignation());
    exp.setLang(def.getLanguage());
    exp.setUuid(uuid.toString());
    return exp;
  }

  /** Converts the given definitions for the given UUID. */
  private List<Definition> convert(List<de.samply.mdr.dal.dto.Definition> definitions, UUID uuid) {
    List<Definition> target = new ArrayList<>();
    for (de.samply.mdr.dal.dto.Definition def : definitions) {
      target.add(convert(def, uuid));
    }
    return target;
  }

  /** Gets the given namespace from the DB or the cache. */
  private de.samply.mdr.dal.dto.Namespace getNamespace(
      DSLContext ctx, String namespace, int userId) {
    if (!namespaceMap.containsKey(namespace)) {
      namespaceMap.put(namespace, NamespaceDao.getNamespace(ctx, userId, namespace));
    }

    return (de.samply.mdr.dal.dto.Namespace) namespaceMap.get(namespace).getElement();
  }
}
