/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.CONCEPT_ASSOCIATION;
import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIER;

import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.tables.Element;
import de.samply.mdr.dal.jooq.tables.Scopedidentifier;
import de.samply.mdr.dal.jooq.tables.records.ConceptAssociationRecord;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.jooq.DSLContext;

/** DAO that returns concept associations. */
public class ConceptAssociationDao {

  /**
   * Saves the given concept association in the database. Expects a concept association linked to an
   * element.
   *
   * @param ca the concept association to store
   */
  public static void saveConceptAssociation(DSLContext ctx, ConceptAssociation ca) {
    ConceptAssociationRecord record = ctx.newRecord(CONCEPT_ASSOCIATION, ca.toJooq());
    ctx.executeInsert(record);
  }

  /**
   * Returns all concept associations for the given element.
   *
   * @param elementUuid the uuid of the element whose concept associations shall be retrieved
   * @return List of concept associations, might be empty.
   */
  public static List<ConceptAssociation> getConceptAssociations(DSLContext ctx, int elementUuid) {
    return ctx.fetch(CONCEPT_ASSOCIATION, CONCEPT_ASSOCIATION.SCOPEDIDENTIFIER_ID.eq(elementUuid))
        .into(de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation.class).stream()
        .map(ConceptAssociation::new).collect(Collectors.toList());
  }

  /**
   * Returns the concept associations for the given URN.
   *
   * @param urn the urn of the element whose concept associations shall be retrieved
   * @return List of concept associations, might be empty.
   */
  public static List<ConceptAssociation> getConceptAssociations(DSLContext ctx, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    de.samply.mdr.dal.jooq.tables.ConceptAssociation ca = CONCEPT_ASSOCIATION.as("ca");
    Scopedidentifier si = SCOPEDIDENTIFIER.as("si");
    Element ns = ELEMENT.as("ns");

    return ctx.select(ca.fields()).from(ca)
        .leftJoin(si).on(ca.SCOPEDIDENTIFIER_ID.eq(si.ID))
        .leftJoin(ns).on(si.NAMESPACEID.eq(ns.ID))
        .where(si.IDENTIFIER.eq(identifier.getIdentifier()))
        .and(si.VERSION.eq(identifier.getVersion()))
        .and(si.ELEMENTTYPE.eq(identifier.getElementType()))
        .and(ns.NAME.eq(identifier.getNamespace()))
        .fetch().into(de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation.class)
        .stream().map(ConceptAssociation::new).collect(Collectors.toList());
  }

  /**
   * Returns all concept associations for the given element for the given systems.
   *
   * @param elementUuid the uuid of the element whose concept associations shall be retrieved
   * @param systems list of systems to search in. If omitted (null or empty list), get all concept
   *     associations
   * @return List of concept associations, might be empty.
   */
  public static List<ConceptAssociation> getConceptAssociations(
      DSLContext ctx, int elementUuid, List<String> systems) {
    if (systems == null || systems.size() == 0) {
      return getConceptAssociations(ctx, elementUuid);
    } else {
      return ctx.fetch(CONCEPT_ASSOCIATION, CONCEPT_ASSOCIATION.SCOPEDIDENTIFIER_ID
          .eq(elementUuid).and(CONCEPT_ASSOCIATION.SYSTEM.in(systems)))
          .into(de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation.class).stream()
          .map(ConceptAssociation::new).collect(Collectors.toList());
    }
  }

  /**
   * Returns the concept association for the given element in the specified system.
   *
   * @param elementUuid the uuid of the element whose concept associations shall be retrieved
   * @param system the system to search in
   * @return List of concept associations, might be empty.
   */
  public static List<ConceptAssociation> getConceptAssociations(
      DSLContext ctx, int elementUuid, String system) {
    List<String> target = new ArrayList<>();
    target.add(system);
    return getConceptAssociations(ctx, elementUuid, target);
  }

  /**
   * Returns the concept association with the given ID.
   *
   * @param id the id of the concept association to get
   * @return the concept association
   */
  public static ConceptAssociation getConceptAssociation(DSLContext ctx, int id) {
    return new ConceptAssociation(ctx.fetchOne(CONCEPT_ASSOCIATION, CONCEPT_ASSOCIATION.ID.eq(id))
        .into(de.samply.mdr.dal.jooq.tables.pojos.ConceptAssociation.class));
  }

}
