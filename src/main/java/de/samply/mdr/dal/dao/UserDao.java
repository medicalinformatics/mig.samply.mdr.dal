/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.MDRUSER;

import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dal.jooq.tables.pojos.Mdruser;
import de.samply.mdr.dal.jooq.tables.records.MdruserRecord;
import java.util.List;
import java.util.stream.Collectors;
import org.jooq.DSLContext;

/** The DAO for users. */
public class UserDao {

  /**
   * Returns the user with the given identity from OAuth2 (OpenID-Connect).
   */
  public static User getUserByIdentity(DSLContext ctx, String identity) {
    try {
      return new User(ctx.fetchOne(MDRUSER, MDRUSER.USERNAME.eq(identity)).into(Mdruser.class));
    } catch (NullPointerException npe) {
      return null;
    }
  }

  /**
   * Returns the user with the given email.
   */
  @Deprecated
  public static User getUserByEmail(DSLContext ctx, String email) {
    Mdruser mdruser = ctx
        .select()
        .from(MDRUSER)
        .where(MDRUSER.DATA + " ->>'" + User.JSON_FIELD_EMAIL + "' = ?", email)
        .fetchOne().into(Mdruser.class);
    return new User(mdruser);
  }

  /**
   * Returns the user with the given ID.
   */
  public static User getUser(DSLContext ctx, int userId) {
    return new User(ctx.fetchOne(MDRUSER, MDRUSER.ID.eq(userId)).into(Mdruser.class));
  }

  /**
   * Returns all users.
   */
  public static List<User> getUsers(DSLContext ctx) {
    return ctx.fetch(MDRUSER).into(Mdruser.class).stream()
        .map(User::new).collect(Collectors.toList());
  }

  /**
   * Updated the users data.
   */
  public static void updateUser(DSLContext ctx, User user) {
    MdruserRecord mdruser = ctx.newRecord(MDRUSER, user.toJooq());
    ctx.executeUpdate(mdruser);
  }

  /**
   * Save the given user.
   */
  private static void saveUser(DSLContext ctx, User user) {
    MdruserRecord mdruser = ctx.newRecord(MDRUSER, user.toJooq());
    mdruser.store();
    user.setId(mdruser.getId());
  }

  /**
   * Creates a new user with the given attributes. Also saves the user.
   */
  public static User createDefaultUser(
      DSLContext ctx, String username, String email, String realName, String externalLabel) {
    User user = new User();
    user.setUsername(username);
    user.setEmail(email);
    user.setRealName(realName);
    user.setExternalLabel(externalLabel);
    saveUser(ctx, user);
    return user;
  }

}
