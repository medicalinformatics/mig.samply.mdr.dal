/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.DEFINITION;
import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.HIERARCHY;
import static de.samply.mdr.dal.jooq.Tables.PERMISSIBLECODE;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIER;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIERHIERARCHY;
import static de.samply.mdr.dal.jooq.Tables.SLOT;
import static de.samply.mdr.dal.jooq.Tables.USER_NAMESPACE_GRANTS;

import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.dal.jooq.tables.pojos.Scopedidentifier;
import de.samply.mdr.dal.jooq.tables.records.HierarchyRecord;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.TableField;
import org.jooq.impl.DSL;

/**
 * High Level DAO used to get identified elements from the MDR.
 *
 * @author paul
 */
public class IdentifiedDao {

  private static SelectOnConditionStep<Record> select(DSLContext ctx) {
    de.samply.mdr.dal.jooq.tables.Element ns = ELEMENT.as("ns");

    return ctx.select(SCOPEDIDENTIFIER.fields())
        .select(ELEMENT.fields())
        .select(DEFINITION.fields())
        .select(ns.NAME)
        .from(SCOPEDIDENTIFIER)
        .leftJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .leftJoin(ELEMENT)
        .on(ELEMENT.ID.eq(SCOPEDIDENTIFIER.ELEMENTID))
        .leftJoin(DEFINITION)
        .on(DEFINITION.ELEMENTID.eq(ELEMENT.ID))
        .and(DEFINITION.SCOPEDIDENTIFIERID.eq(SCOPEDIDENTIFIER.ID));
  }

  /**
   * Fetch and convert IdentifiedElements from a given query.
   */
  private static List<IdentifiedElement> fetch(Select<Record> query) {
    Result<?> result = query.fetch();
    Map<Integer, IdentifiedElement> identifiedElements = new HashMap<>();
    de.samply.mdr.dal.jooq.tables.Element ns = ELEMENT.as("ns");

    for (Record r : result) {
      ScopedIdentifier scopedIdentifier =
          new ScopedIdentifier(r.into(SCOPEDIDENTIFIER.fields()).into(Scopedidentifier.class),
              r.get(ns.NAME));
      de.samply.mdr.dal.jooq.tables.pojos.Element element =
          r.into(ELEMENT.fields()).into(de.samply.mdr.dal.jooq.tables.pojos.Element.class);
      de.samply.mdr.dal.jooq.tables.pojos.Definition definition =
          r.into(DEFINITION.fields()).into(de.samply.mdr.dal.jooq.tables.pojos.Definition.class);

      if (identifiedElements.containsKey(scopedIdentifier.getId())) {
        identifiedElements.get(scopedIdentifier.getId()).getDefinitions()
            .add(new Definition(definition));
      } else {
        IdentifiedElement identifiedElement = new IdentifiedElement(scopedIdentifier);
        identifiedElement.setElement(ElementDao.getObject(element));
        identifiedElement.getDefinitions().add(new Definition(definition));
        identifiedElements.put(scopedIdentifier.getId(), identifiedElement);
      }
    }

    return new ArrayList<>(identifiedElements.values());
  }

  /**
   * Searches the database for elements with the specified text, slots and status.
   */
  public static List<IdentifiedElement> findElements(
      DSLContext ctx,
      int userId,
      String input,
      String selectedNamespace,
      Elementtype[] selectedTypes,
      Status[] selectedStatuses,
      HashMap<String, String> slots) {

    List<Condition> conditions = new ArrayList<>();

    if (input.trim().length() > 0) {
      conditions.add(SCOPEDIDENTIFIER.ID.in(
          ctx.selectDistinct(DEFINITION.SCOPEDIDENTIFIERID)
              .from(DEFINITION)
              .where(DEFINITION.DEFINITION_.likeIgnoreCase("%" + input + "%"))
              .or(DEFINITION.DESIGNATION.likeIgnoreCase("%" + input + "%"))
      ));
    }

    if (!StringUtil.isEmpty(selectedNamespace)) {
      conditions.add(ELEMENT.as("ns").NAME.eq(selectedNamespace));
    }

    if (selectedTypes.length > 0) {
      conditions.add(SCOPEDIDENTIFIER.ELEMENTTYPE.in(selectedTypes));
    }

    if (selectedStatuses.length > 0) {
      conditions.add(SCOPEDIDENTIFIER.STATUS.in(selectedStatuses));
    }

    if (slots.keySet().size() > 0) {
      List<Condition> subconditions = new ArrayList<>();
      // TODO: shouldn't this be key=key and value=value for the subcondition?
      slots.forEach((key, value)
          -> subconditions.add(SLOT.KEY.eq(value).and(SLOT.VALUE.eq(value))));
      conditions.add(SCOPEDIDENTIFIER.ID.in(
          ctx.select(SLOT.SCOPEDIDENTIFIERID)
              .from(SLOT)
              .where(DSL.or(subconditions))
      ));
    }

    return fetch(select(ctx)
        .where(conditions)
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Returns a list of all available catalogs.
   */
  public static List<IdentifiedElement> getCatalogs(DSLContext ctx,int userId) {
    return fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(Elementtype.CATALOG))
        .and(SCOPEDIDENTIFIER.STATUS.eq(Status.RELEASED))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Returns the specified element.
   */
  public static IdentifiedElement getElement(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);

    if (identifier.getVersion().toLowerCase().equals("latest")) {
      return getLatestElement(ctx, userId, urn);
    }

    List<IdentifiedElement> identifiedElements = fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
        .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))
        .and(ELEMENT.as("ns").NAME.eq(identifier.getNamespace()))
        .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));

    return identifiedElements.stream().findFirst().orElse(null);
  }

  /**
   * Returns the element for the given scoped identifier ID.
   */
  public static IdentifiedElement getElement(DSLContext ctx,int userId, int scopedIdentifierId) {
    List<IdentifiedElement> identifiedElements = fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.ID.eq(scopedIdentifierId))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));

    return identifiedElements.stream().findFirst().orElse(null);
  }

  /** Returns the latest released element of the specified URN. */
  public static IdentifiedElement getLatestElement(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier latestVersion = ScopedIdentifierDao.getLatestVersion(ctx, urn);
    // TODO: is latestVersion ever null or is it an empty object?
    if (latestVersion != null) {
      return getElement(ctx, userId, latestVersion.toString());
    } else {
      return null;
    }
  }

  /**
   * Returns the specified element.
   */
  public static IdentifiedElement getElementById(DSLContext ctx, int userId, int elementId) {
    List<IdentifiedElement> identifiedElements = fetch(select(ctx)
        .where(ELEMENT.ID.eq(elementId))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));

    return identifiedElements.stream().findFirst().orElse(null);
  }

  /**
   * Returns the specified element.
   *
   * @param dataelementUrn the urn of the dataelement in which to look for the element
   * @param codeValue the code itself
   * @return the code element
   */
  public static IdentifiedElement getElementByCode(
      DSLContext ctx, int userId, String dataelementUrn, String codeValue) {
    // First, get the dataelement and check if it has a catalog as value domain
    final ScopedIdentifier dataelementIdentifier = ScopedIdentifier.fromUrn(dataelementUrn);
    IdentifiedElement element = getElement(ctx, userId, dataelementUrn);
    if (element == null) {
      return null;
    }
    DataElement dataelement = (DataElement) element.getElement();
    Element valueDomain = ElementDao.getElement(ctx, dataelement.getValueDomainId());

    if (!(valueDomain instanceof CatalogValueDomain)) {
      return null;
    }

    List<IdentifiedElement> identifiedElements = fetch(select(ctx)
        .leftJoin(HIERARCHY)
        .on(HIERARCHY.SUB.eq(SCOPEDIDENTIFIER.ID))
        .where(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(Elementtype.CODE))
        .and(ELEMENT.CODE.eq(codeValue))
        .and(ELEMENT.as("ns").NAME.eq(dataelementIdentifier.getNamespace()))
        .and(HIERARCHY.ROOT.eq(((CatalogValueDomain) valueDomain).getCatalogScopedIdentifierId()))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));

    return identifiedElements.stream().findFirst().orElse(null);
  }

  /**
   * Returns all elements that are currently in a draft status and writable by the user.
   */
  public static List<IdentifiedElement> getDrafts(DSLContext ctx, int userId) {
    return fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.ELEMENTTYPE.notEqual(Elementtype.CODE))
        .and(SCOPEDIDENTIFIER.STATUS.eq(Status.DRAFT))
        .and(SCOPEDIDENTIFIER.NAMESPACEID.in(
            ctx.select(USER_NAMESPACE_GRANTS.NAMESPACE_ID)
                .from(USER_NAMESPACE_GRANTS)
                .where(USER_NAMESPACE_GRANTS.USER_ID.eq(userId))
        ))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Returns all root elements that aren't in a group, catalog, or code and that are released.
   */
  public static List<IdentifiedElement> getRootElements(
      DSLContext ctx, int userId, String namespace) {
    return getRootElements(ctx, userId, namespace, Status.RELEASED);
  }

  /**
   * Returns all root elements that aren't in a group, catalog, or code and have a specific status.
   */
  public static List<IdentifiedElement> getRootElements(
      DSLContext ctx, int userId, String namespace, Status status) {
    return getRootElements(ctx, userId, namespace, Collections.singletonList(status));
  }

  /**
   * Returns all root elements that aren't in a group, catalog, or code and have a specific status.
   */
  public static List<IdentifiedElement> getRootElements(
      DSLContext ctx, int userId, String namespace, List<Status> status) {
    de.samply.mdr.dal.jooq.tables.Element ns = ELEMENT.as("ns");
    return fetch(select(ctx)
        .where(ns.NAME.eq(namespace))
        .and(SCOPEDIDENTIFIER.STATUS.in(status))
        .and(SCOPEDIDENTIFIER.ID.notIn(
            ctx.select(SCOPEDIDENTIFIERHIERARCHY.SUBID)
                .from(SCOPEDIDENTIFIERHIERARCHY)
                .leftJoin(SCOPEDIDENTIFIER)
                .on(SCOPEDIDENTIFIER.ID.eq(SCOPEDIDENTIFIERHIERARCHY.SUPERID))
                .leftJoin(ns)
                .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
                .where(ns.NAME.eq(namespace))
                .and(SCOPEDIDENTIFIER.STATUS.in(status))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(Elementtype.DATAELEMENTGROUP)
                    .or(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(Elementtype.CATALOG))
                    .or(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(Elementtype.CODE)))
        ))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Returns all released elements accessible by a user.
   */
  public static List<IdentifiedElement> getElements(DSLContext ctx, int userId) {
    return fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.STATUS.eq(Status.RELEASED))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Returns a list of identified elements that described the same element as the specified URN.
   */
  public static List<IdentifiedElement> getSimilarElements(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier identifier = ScopedIdentifierDao.getScopedIdentifier(ctx, userId, urn);
    return fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.ELEMENTID.eq(identifier.getElementId()))
        .and(SCOPEDIDENTIFIER.UUID.eq(identifier.getUuid()))
        .and(SCOPEDIDENTIFIER.STATUS.eq(Status.RELEASED))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Finds the Owned representations for the given URN with the given statuses.
   */
  public static List<IdentifiedElement> findOwnedRepresentations(
      DSLContext ctx, int userId, String urn, Status... status) {
    ScopedIdentifier identifier = ScopedIdentifierDao.getScopedIdentifier(ctx, userId, urn);

    SelectConditionStep<Record> query = select(ctx)
        .where(SCOPEDIDENTIFIER.ELEMENTID.eq(identifier.getElementId()))
        .and(SCOPEDIDENTIFIER.NAMESPACEID.in(
            ctx.select(USER_NAMESPACE_GRANTS.NAMESPACE_ID)
                .from(USER_NAMESPACE_GRANTS)
                .where(USER_NAMESPACE_GRANTS.USER_ID.eq(userId))
        ))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns")));

    if (status != null && status.length > 0) {
      query = query.and(SCOPEDIDENTIFIER.STATUS.in(status));
    }

    return fetch(query.and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Returns all members from the given URN.
   */
  public static List<IdentifiedElement> getSubMembers(DSLContext ctx, int userId, String urn) {
    return getEntries(ctx, userId, urn);
  }

  /**
   * Returns all entries (= ordered members) from the given URN.
   */
  public static List<IdentifiedElement> getEntries(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    de.samply.mdr.dal.jooq.tables.Scopedidentifier sc2 = SCOPEDIDENTIFIER.as("sc2");
    de.samply.mdr.dal.jooq.tables.Element n2 = ELEMENT.as("n2");

    return fetch(select(ctx)
        .leftJoin(SCOPEDIDENTIFIERHIERARCHY)
        .on(SCOPEDIDENTIFIERHIERARCHY.SUBID.eq(SCOPEDIDENTIFIER.ID))
        .leftJoin(sc2)
        .on(sc2.ID.eq(SCOPEDIDENTIFIERHIERARCHY.SUPERID))
        .leftJoin(n2)
        .on(n2.ID.eq(sc2.NAMESPACEID))
        .where(sc2.IDENTIFIER.eq(identifier.getIdentifier()))
        .and(sc2.VERSION.eq(identifier.getVersion()))
        .and(sc2.ELEMENTTYPE.eq(identifier.getElementType()))
        .and(n2.NAME.eq(identifier.getNamespace()))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns")))
        .orderBy(SCOPEDIDENTIFIERHIERARCHY.ORDER));
  }

  /**
   * Returns all submembers of the given URN. Includes really *all* descendants.
   *
   * @param locales list of all requested languages
   */
  public static List<IdentifiedElement> getAllSubMembers(
      DSLContext ctx, int userId, String urn, List<String> locales) {

    if (locales == null || locales.isEmpty()) {
      return getAllSubMembers(ctx, userId, urn);
    }

    Builder<String> builderWithSingleQuotes =
        new Builder<String>() {
          @Override
          public String build(String s) {
            return "\'" + s + "\'";
          }
        };

    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);

    de.samply.mdr.dal.jooq.tables.Element ns = ELEMENT.as("ns");
    return fetch(select(ctx)
        .where(DEFINITION.LANGUAGE.in(StringUtil.join(locales, ",", builderWithSingleQuotes)))
        .and(SCOPEDIDENTIFIER.ID.in(
            ctx.select(HIERARCHY.SUB)
                .from(HIERARCHY)
                .leftJoin(SCOPEDIDENTIFIER)
                .on(SCOPEDIDENTIFIER.ID.eq(HIERARCHY.ROOT))
                .leftJoin(ns)
                .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
                .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
                .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
                .and(ns.NAME.eq(identifier.getNamespace()))
        ))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ns))
        .orderBy(SCOPEDIDENTIFIERHIERARCHY.ORDER));
  }

  /**
   * Returns all submembers of the given URN. Includes really *all* descendants.
   */
  public static List<IdentifiedElement> getAllSubMembers(DSLContext ctx, int userId, String urn) {
    return getDescendants(ctx, userId, urn, HIERARCHY.ROOT);
  }

  /**
   * Returns all direct children of the given URN. Includes only the first level of descendants.
   */
  public static List<IdentifiedElement> getChildren(DSLContext ctx, int userId, String urn) {
    return getDescendants(ctx, userId, urn, HIERARCHY.SUPER);
  }

  private static List<IdentifiedElement> getDescendants(
      DSLContext ctx, int userId, String urn, TableField<HierarchyRecord, Integer> hierarchyField) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    de.samply.mdr.dal.jooq.tables.Element ns = ELEMENT.as("ns");
    return fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.ID.in(
            ctx.select(HIERARCHY.SUB)
                .from(HIERARCHY)
                .leftJoin(SCOPEDIDENTIFIER)
                .on(hierarchyField.eq(SCOPEDIDENTIFIER.ID))
                .leftJoin(ns)
                .on(SCOPEDIDENTIFIER.NAMESPACEID.eq(ns.ID))
                .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
                .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))
                .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
                .and(ns.NAME.eq(identifier.getNamespace()))
        ))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * Returns all permissible codes for the given value domain.
   */
  public static List<IdentifiedElement> getPermissibleCodes(
      DSLContext ctx, int userId, ValueDomain valueDomain) {
    return fetch(select(ctx)
        .where(SCOPEDIDENTIFIER.ID.in(
            ctx.select(PERMISSIBLECODE.CODESCOPEDIDENTIFIERID)
                .from(PERMISSIBLECODE)
                .where(PERMISSIBLECODE.CATALOGVALUEDOMAINID.eq(valueDomain.getId()))
        ))
        .and(DaoUtil.accessibleByUserId(ctx, userId, ELEMENT.as("ns"))));
  }

  /**
   * This method deletes the given URN from the database. Only possible for dataelements,
   * dataelementgroups, and records. The method does *not* delete the element, rather it deletes the
   * definitions and slots for the scoped identifier and the scoped identifier itself.
   */
  public static void delete(DSLContext ctx, int userId, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);

    /* Disable this method for users with a non-zero user ID */
    if (userId != 0) {
      throw new UnsupportedOperationException("You can not call delete with a non-zero user ID");
    }

    if (identifier.getElementType() == Elementtype.DATAELEMENT
        || identifier.getElementType() == Elementtype.DATAELEMENTGROUP
        || identifier.getElementType() == Elementtype.RECORD) {

      IdentifiedElement element = IdentifiedDao.getElement(ctx, userId, urn);

      final boolean previousCommitSetting = ctx.connectionResult(c -> {
        boolean commit = c.getAutoCommit();
        c.setAutoCommit(false);
        return commit;
      });

      ctx.deleteFrom(DEFINITION)
          .where(DEFINITION.SCOPEDIDENTIFIERID.eq(element.getScoped().getId())).execute();

      ctx.deleteFrom(SCOPEDIDENTIFIERHIERARCHY)
          .where(SCOPEDIDENTIFIERHIERARCHY.SUPERID.eq(element.getScoped().getId()));

      ctx.deleteFrom(SCOPEDIDENTIFIER)
          .where(SCOPEDIDENTIFIER.ID.eq(element.getScoped().getId()));

      ctx.connection(c -> {
        c.commit();
        c.setAutoCommit(previousCommitSetting);
      });
    }
  }

  /**
   * This method imports the given URN into the namespace "newNamespace". It checks, if it already
   * has been imported by using the uuid. If it has not been imported yet, check if the same element
   * has been imported in a different version. If it has, create a new revision with the same
   * identifier. If it has never been imported, create a new identifier for the same element
   */
  public static ScopedIdentifier importElement(
      DSLContext ctx, String urn, String newNamespace, int userId) {
    return duplicate(ctx, urn, newNamespace, userId, false);
  }

  /**
   * Duplicates the specified URN.
   *
   * @param existing if true, a new urn will be created, even if an identical scoped identifier
   *     exists.
   */
  public static ScopedIdentifier duplicate(
      DSLContext ctx, String urn, String newNamespace, int userId, boolean existing) {
    if (!existing) {
      ScopedIdentifier importedIdentifier =
          ScopedIdentifierDao.getIdenticalScopedIdentifier(ctx, urn, newNamespace, userId);
      if (importedIdentifier != null) {
        return importedIdentifier;
      }
    }

    IdentifiedElement element = IdentifiedDao.getElement(ctx, userId, urn);
    Namespace ns = ElementDao.getNamespace(ctx, newNamespace);

    ScopedIdentifier identifier = element.getScoped();

    ScopedIdentifier newIdentifier = new ScopedIdentifier();
    newIdentifier.setCreatedBy(userId);
    newIdentifier.setNamespace(newNamespace);
    newIdentifier.setIdentifier(
        ScopedIdentifierDao.getFreeIdentifier(ctx, newNamespace, identifier.getElementType()));
    newIdentifier.setVersion("1");
    newIdentifier.setElementId(identifier.getElementId());
    newIdentifier.setStatus(identifier.getStatus());
    newIdentifier.setUrl(identifier.toString());
    newIdentifier.setElementType(identifier.getElementType());
    newIdentifier.setNamespaceId(ns.getId());
    newIdentifier.setUuid(identifier.getUuid());

    if (!existing) {
      ScopedIdentifier importedIdentifier =
          ScopedIdentifierDao.getScopedIdentifierForElement(ctx, urn, newNamespace, userId);
      if (importedIdentifier != null) {
        /*
         * So the user has at some point already imported this element. Create a new revision of
         * this Scoped Identifier and store it.
         */
        newIdentifier.setVersion(nextRevision(importedIdentifier.getVersion()));
        newIdentifier.setIdentifier(importedIdentifier.getIdentifier());
      }
    }

    newIdentifier.setId(ScopedIdentifierDao.saveScopedIdentifier(ctx, newIdentifier));

    for (Slot s : SlotDao.getSlots(ctx, identifier.toString())) {
      Slot newSlot = new Slot();
      newSlot.setKey(s.getKey());
      newSlot.setValue(s.getValue());
      newSlot.setScopedIdentifierId(newIdentifier.getId());
      SlotDao.saveSlot(ctx, newSlot);
    }

    for (Definition definition : element.getDefinitions()) {
      Definition newDefinition = new Definition();
      newDefinition.setDefinition(definition.getDefinition());
      newDefinition.setDesignation(definition.getDesignation());
      newDefinition.setElementId(definition.getElementId());
      newDefinition.setLanguage(definition.getLanguage());
      newDefinition.setScopedIdentifierId(newIdentifier.getId());

      DefinitionDao.saveDefinition(ctx, newDefinition);
    }

    if (identifier.getElementType() == Elementtype.DATAELEMENT) {
      DataElement dataElement = (DataElement) ElementDao.getElement(ctx, identifier.getElementId());
      ValueDomain domain = (ValueDomain) ElementDao.getElement(ctx, dataElement.getValueDomainId());

      /* If it is an enumerated value domain, copy every definition into the new namespace */
      if (domain instanceof EnumeratedValueDomain) {
        List<Element> values = ElementDao.getPermissibleValues(ctx, domain.getId());
        for (Element value : values) {
          List<Definition> definitions =
              DefinitionDao.getDefinitions(ctx, value.getId(), identifier.getId());

          for (Definition definition : definitions) {
            Definition newDefinition = new Definition();
            newDefinition.setDefinition(definition.getDefinition());
            newDefinition.setDesignation(definition.getDesignation());
            newDefinition.setElementId(definition.getElementId());
            newDefinition.setLanguage(definition.getLanguage());
            newDefinition.setScopedIdentifierId(newIdentifier.getId());

            DefinitionDao.saveDefinition(ctx, newDefinition);
          }
        }
      }
    }

    return newIdentifier;
  }


  /** Returns the next revision for the given version. */
  private static String nextRevision(String version) {
    try {
      return "" + (Integer.parseInt(version) + 1);
    } catch (Exception e) {
      return "1";
    }
  }

}
