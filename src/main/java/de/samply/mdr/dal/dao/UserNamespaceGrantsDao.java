/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.USER_NAMESPACE_GRANTS;

import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.NamespacePermission;
import de.samply.mdr.dal.dto.NamespacePermission.Permission;
import de.samply.mdr.dal.jooq.enums.GrantType;
import de.samply.mdr.dal.jooq.tables.pojos.UserNamespaceGrants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;

public class UserNamespaceGrantsDao {

  /** Returns a list of Namespace Permissions. */
  public static List<NamespacePermission> getPermissions(DSLContext ctx, Namespace namespace) {
    List<NamespacePermission> permissions = ctx.fetch(USER_NAMESPACE_GRANTS,
        USER_NAMESPACE_GRANTS.NAMESPACE_ID.eq(namespace.getId()))
        .into(UserNamespaceGrants.class).stream()
        .map(NamespacePermission::new).collect(Collectors.toList());

    // TODO: It seems that for each GrantType also the lower (inherited) GrantTypes are saved.
    //   Therefore, we need to find the highest GrantType here.
    Map<Integer, NamespacePermission> accu = new HashMap<>();

    for (NamespacePermission permission : permissions) {
      Permission current = permission.getPermission();
      NamespacePermission tmp = accu.get(permission.getUserId());
      Permission saved = (tmp == null) ? null : tmp.getPermission();

      if (saved == null
          || (saved == Permission.READ && current != Permission.READ)
          || (saved == Permission.READ_WRITE && current == Permission.READ_WRITE_ADMIN)
      ) {
        accu.put(permission.getUserId(), permission);
      }
    }

    return new ArrayList<>(accu.values());
  }

  /** Update the permissions to the given namespace. */
  public static void updatePermissions(
      DSLContext ctx, List<NamespacePermission> permissions, Namespace ns) {
    ctx.deleteFrom(USER_NAMESPACE_GRANTS)
        .where(USER_NAMESPACE_GRANTS.NAMESPACE_ID.eq(ns.getId())).execute();

    // TODO: It seems that for each GrantType also the lower (inherited) GrantTypes are saved.
    for (NamespacePermission permission : permissions) {
      UserNamespaceGrants grant = permission.toJooq();
      ctx.insertInto(USER_NAMESPACE_GRANTS)
          .values(grant.getUserId(), grant.getNamespaceId(), grant.getGrantType()).execute();

      if (permission.getPermission() == Permission.READ_WRITE) {
        ctx.insertInto(USER_NAMESPACE_GRANTS)
            .values(grant.getUserId(), grant.getNamespaceId(), GrantType.READ).execute();
      }

      if (permission.getPermission() == Permission.READ_WRITE_ADMIN) {
        ctx.insertInto(USER_NAMESPACE_GRANTS)
            .values(grant.getUserId(), grant.getNamespaceId(), GrantType.READ).execute();
        ctx.insertInto(USER_NAMESPACE_GRANTS)
            .values(grant.getUserId(), grant.getNamespaceId(), GrantType.WRITE).execute();
      }
    }
  }

}
