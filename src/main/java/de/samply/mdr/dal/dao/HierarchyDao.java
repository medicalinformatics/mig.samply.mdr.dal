/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.HIERARCHY;
import static de.samply.mdr.dal.jooq.Tables.PERMISSIBLECODE;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIER;

import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.tables.Element;
import de.samply.mdr.dal.jooq.tables.pojos.Hierarchy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;

public class HierarchyDao {

  /** Returns all hierarchy nodes for the given URN. */
  public static List<HierarchyNode> getHierarchyNodes(DSLContext ctx, String urn) {
    return getHierarchyNodes(ctx, urn, false);
  }

  /**
   * Returns all hierarchy nodes for the given URN.
   *
   * @param urn the urn of the catalog or code
   * @param directDescendantsOnly if true, get only those nodes that have the element defined by the
   *     urn as "super" entry, if false, get those that have that element as "root" entry
   */
  public static List<HierarchyNode> getHierarchyNodes(
      DSLContext ctx, String urn, boolean directDescendantsOnly) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);

    Element ns = ELEMENT.as("ns");

    SelectConditionStep<Record> query = ctx.select(HIERARCHY.fields())
        .from(HIERARCHY)
        .leftJoin(SCOPEDIDENTIFIER)
        .on(SCOPEDIDENTIFIER.ID.eq(HIERARCHY.ROOT))
        .leftJoin(ns)
        .on(ns.ID.eq(SCOPEDIDENTIFIER.NAMESPACEID))
        .where(SCOPEDIDENTIFIER.IDENTIFIER.eq(identifier.getIdentifier()))
        .and(SCOPEDIDENTIFIER.VERSION.eq(identifier.getVersion()))
        .and(SCOPEDIDENTIFIER.ELEMENTTYPE.eq(identifier.getElementType()))
        .and(ns.NAME.eq(identifier.getNamespace()));

    if (directDescendantsOnly) {
      query = query.and(HIERARCHY.ROOT.eq(HIERARCHY.SUPER));
    }

    return query.fetch().into(Hierarchy.class).stream()
        .map(HierarchyNode::new).collect(Collectors.toList());
  }

  /**
   * Returns if a node is also having child nodes.
   *
   * @param catalogValueDomainId id of the catalog value domain this is checked for
   * @param ids a list of the ids to check
   */
  public static Map<Integer, Boolean> getNodesWithChildren(
      DSLContext ctx, int catalogValueDomainId, List<Integer> ids) {
    Map<Integer, Boolean> nodesWithChildren = new HashMap<>();

    if (ids == null || ids.isEmpty()) {
      return nodesWithChildren;
    }

    List<Integer> hierarchyNodes = ctx.selectDistinct(HIERARCHY.SUPER)
        .from(HIERARCHY)
        .join(PERMISSIBLECODE)
        .on(HIERARCHY.SUB.eq(PERMISSIBLECODE.CODESCOPEDIDENTIFIERID))
        .where(HIERARCHY.SUPER.in(ids))
        .and(PERMISSIBLECODE.CATALOGVALUEDOMAINID.eq(catalogValueDomainId))
        .fetch().stream().map(Record1::value1).collect(Collectors.toList());

    for (Integer i : ids) {
      nodesWithChildren.put(i, hierarchyNodes.contains(i));
    }

    return nodesWithChildren;
  }
}
