/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.DEFINITION;

import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.jooq.tables.records.DefinitionRecord;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.jooq.DSLContext;

/**
 * DAO that returns Definitions.
 *
 * @author paul
 */
public class DefinitionDao {

  /**
   * Saves the given definition in the database. Expects a definition linked to an element.
   */
  public static void saveDefinition(DSLContext ctx, Definition def) {
    DefinitionRecord record = ctx.newRecord(DEFINITION, def.toJooq());
    ctx.executeInsert(record);
  }

  /**
   * Saves the given definition in the database. Expects a definition linked to a namespace.
   */
  public static void saveNamespaceDefinition(DSLContext ctx, Definition def) {
    DefinitionRecord record = ctx.newRecord(DEFINITION, def.toJooq());
    ctx.executeInsert(record);
  }

  /**
   * Returns all definitions for the given element and scoped identifier.
   *
   * @return Array of definitions, might be empty.
   */
  public static List<Definition> getDefinitions(
      DSLContext ctx, int elementId, int scopedIdentifierId) {
    return ctx.fetch(DEFINITION,DEFINITION.ELEMENTID.eq(elementId)
        .and(DEFINITION.SCOPEDIDENTIFIERID.eq(scopedIdentifierId)))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Definition.class).stream().map(Definition::new)
        .collect(Collectors.toList());
  }

  /**
   * Returns all definitions for the given element and scoped identifier for the given languages.
   *
   * @return Array of definitions, might be empty.
   */
  public static List<Definition> getDefinitions(
      DSLContext ctx, int elementId, int scopedIdentifierId, List<String> languages) {
    if (languages == null || languages.size() == 0) {
      return getDefinitions(ctx, elementId, scopedIdentifierId);
    } else {
      return ctx.fetch(DEFINITION,DEFINITION.ELEMENTID.eq(elementId)
          .and(DEFINITION.SCOPEDIDENTIFIERID.eq(scopedIdentifierId)
          .and(DEFINITION.LANGUAGE.in(languages))))
          .into(de.samply.mdr.dal.jooq.tables.pojos.Definition.class).stream()
          .map(Definition::new).collect(Collectors.toList());
    }
  }

  /**
   * Returns the definition for the given element and scoped identifier in the specified language.
   */
  public static List<Definition> getDefinitions(
      DSLContext ctx, int elementId, int scopedIdentifierId, String language) {
    List<String> target = new ArrayList<>();
    target.add(language);
    return getDefinitions(ctx, elementId, scopedIdentifierId, target);
  }

  /**
   * Returns the definition with the given ID.
   */
  public static Definition getDefinition(DSLContext ctx, int id) {
    return new Definition(ctx.fetchOne(DEFINITION, DEFINITION.ID.eq(id))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Definition.class));
  }

  /**
   * Delete all definitions for a given element id.
   */
  public static void deleteDefinitionsByElementId(DSLContext ctx, int elementId) {
    ctx.deleteFrom(DEFINITION).where(DEFINITION.ELEMENTID.eq(elementId)).execute();
  }

}
