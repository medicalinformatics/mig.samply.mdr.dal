/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Recommendation;
import de.samply.mdr.dal.dto.Recommendation.Definition;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.jooq.DSLContext;

public class RecommendationDao {

  /**
   * Compute the list of 2-grams for a given String. For example the String 'abcdef' results in the
   * list ['ab', 'bc', 'cd', 'de', 'ef'].
   *
   * @param str The string that is split into 2-grams
   * @return The list of 2-grams
   */
  private static Set<String> twoGrams(String str) {
    Set<String> tgrams = new HashSet<>();

    if (str != null) {
      for (int i = 0; i < str.length() - 1; i++) {
        tgrams.add(str.substring(i, i + 2));
      }
    }

    return tgrams;
  }

  public static List<Recommendation> getRecommendedElements(
      DSLContext ctx, int userId, String searchStr) {
    return getRecommendedElements(ctx, userId, searchStr, null, 0, 0);
  }

  public static List<Recommendation> getRecommendedElements(
      DSLContext ctx, int userId, String searchStr, int limit, int threshold) {
    return getRecommendedElements(ctx, userId, searchStr, null, limit, threshold);
  }

  /**
   * Find recommended dataelements for a given String in a given language. The returned list size
   * can be controlled by limit and by threshold.
   *
   * @param searchStr The String for which recommended elements are searched.
   * @param languages The languages which the recommendations should be part of.
   * @param limit Maximum list size, can be exceeded by threshold if there are multiple
   *     recommendations with the same maximum similarity. Can be zero or negative if no limit
   *     should be used.
   * @param threshold Minimum similarity of the recommendations. Between 0 and 100.
   * @return A list of recommendations.
   */
  public static List<Recommendation> getRecommendedElements(
      DSLContext ctx, int userId, String searchStr, List<String> languages, int limit,
      int threshold) {

    // If nothing is searched we can immediately return an empty list
    if (searchStr == null || searchStr.equals("")) {
      return Collections.emptyList();
    }

    List<Recommendation> recommendations = queryRecommendations(ctx, userId, searchStr, languages);

    sortRecommendations(recommendations);

    recommendations = mergeMultilingualRecommendations(recommendations);

    return filterRecommendations(recommendations, limit, threshold);
  }

  /**
   * Fetch a list of recommendations from the database for a given String, filtered by a list of
   * languages. Get all definitions and designations that are not hidden and compute the similiarty
   * compared to the given String.
   *
   * @param languages The list of languages, if null, empty or containing only '*' it is ignored and
   *     all languages are used.
   */
  private static List<Recommendation> queryRecommendations(
      DSLContext ctx, int userId, String searchedString, List<String> languages) {
    List<Recommendation> recommendations = new ArrayList<>();
    List<IdentifiedElement> elements = IdentifiedDao.getElements(ctx, userId);

    for (IdentifiedElement element : elements) {
      for (de.samply.mdr.dal.dto.Definition definition : element.getDefinitions()) {
        if (languages == null || languages.isEmpty()
            || languages.stream().anyMatch(l -> l.equals("*"))
            || languages.stream().anyMatch(l -> l.equals(definition.getLanguage()))) {
          Recommendation recommendation = new Recommendation();
          recommendation.setUrn(element.getScoped().toString());
          recommendation.setType(element.getElementType().getName());
          recommendation.setLanguage(definition.getLanguage());
          recommendation.setDesignation(definition.getDesignation());
          recommendation.setDefinition(definition.getDefinition());
          recommendation.setDesignationSimilarity(
              similarity(searchedString, definition.getDesignation()));
          recommendation.setDefinitionSimilarity(
              similarity(searchedString, definition.getDefinition()));
          recommendations.add(recommendation);
        }
      }
    }

    return recommendations;
  }

  /**
   * Sort list of recommendations by the highest value of both definition or designation similarity.
   */
  private static void sortRecommendations(List<Recommendation> recommendations) {
    // Sort the recommendations by the maximum of definition and designation similarity
    Collections.sort(
        recommendations,
        (r1, r2) -> {
          float sim1 =
              r1.getDefinitionSimilarity() > r1.getDesignationSimilarity()
                  ? r1.getDefinitionSimilarity()
                  : r1.getDesignationSimilarity();
          float sim2 =
              r2.getDefinitionSimilarity() > r2.getDesignationSimilarity()
                  ? r2.getDefinitionSimilarity()
                  : r2.getDesignationSimilarity();
          float dist = sim1 - sim2;

          if (dist > 0.0) {
            return -1;
          } else if (dist == 0.0) {
            return 0;
          } else {
            return 1;
          }
        });
  }

  /**
   * Merge recommendations with different definitions for the same element. The recommendations are
   * merged into the object with the highest similarity. Must be sorted previously.
   */
  private static List<Recommendation> mergeMultilingualRecommendations(
      List<Recommendation> recommendations) {

    List<Recommendation> merged = new ArrayList<>();
    List<String> seenIds = new ArrayList<>();

    for (Recommendation r : recommendations) {
      String id = r.getUrn();
      if (!seenIds.contains(id)) {
        merged.add(r);
        seenIds.add(id);
      } else {
        Recommendation match = merged.get(seenIds.indexOf(id));
        match.getDefinitions().add(new Definition(r));
      }
    }

    return merged;
  }

  /**
   * Filter a list of recommendations by a given limit and a given threshold. Return a new, filtered
   * list. Must be sorted previously.
   *
   * @param limit The limit of returned recommendations. If the lowest similarity occurs multiple
   *     times the limit is expanded to include all those recommendations.
   * @param threshold The minimum similarity given in percent (0-100), inclusive threshold value.
   * @return The list of recommendations filtered by limit and threshold.
   */
  private static List<Recommendation> filterRecommendations(
      List<Recommendation> recommendations, int limit, int threshold) {

    if (limit <= 0) {
      // If limit is zero or negative we don't want to use a limit
      limit = recommendations.size();
    }

    List<Recommendation> filtered = new ArrayList<>();

    // This requires a previously sorted list.
    float previousMaxSim = 1;
    for (int i = 0; i < recommendations.size(); i++) {
      Recommendation r = recommendations.get(i);
      float defSim = r.getDefinitionSimilarity();
      float desSim = r.getDesignationSimilarity();

      if (i < limit && (defSim >= threshold || desSim >= threshold)) {
        // Limit not reached and similarity is over threshold
        filtered.add(r);
        previousMaxSim = defSim > desSim ? defSim : desSim;
      } else if (defSim == previousMaxSim || desSim == previousMaxSim) {
        // Limit might be reached, however we still add until all elements which have the same
        // similarity as the last element are added
        filtered.add(r);
      } else {
        // We reached the limit and threshold, stop iterating
        break;
      }
    }

    return filtered;
  }

  /**
   * Compute the similarity of two given Strings by using their 2-grams. The similarity is given in
   * percent (0-100).
   *
   * @return The similarity between 0 and 100.
   */
  private static int similarity(String s1, String s2) {
    Set<String> grams1 = twoGrams(s1);
    Set<String> grams2 = twoGrams(s2);
    Set<String> set = new HashSet<>(grams1);
    set.retainAll(new HashSet<>(grams2));
    return (int) Math.ceil(100.0 * 2.0 * set.size() / (grams1.size() + grams2.size()));
  }
}
