/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.IMPORT;
import static de.samply.mdr.dal.jooq.Tables.USER_NAMESPACE_GRANTS;

import de.samply.mdr.dal.dto.Import;
import de.samply.mdr.dal.jooq.enums.GrantType;
import de.samply.mdr.dal.jooq.tables.records.ImportRecord;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;

/** The DAO that can be used to get import elements. */
public class ImportDao {

  /**
   * Returns a condition which checks whether a user has the required grant to a namespace or not.
   */
  private static SelectConditionStep<Record1<Integer>> getUserNamespaceGrantsQuery(
      DSLContext ctx, int userId, GrantType granttype) {
    return ctx.select(USER_NAMESPACE_GRANTS.NAMESPACE_ID)
        .from(USER_NAMESPACE_GRANTS)
        .where(USER_NAMESPACE_GRANTS.USER_ID.eq(userId))
        .and(USER_NAMESPACE_GRANTS.GRANT_TYPE.eq(granttype));
  }

  /** Returns all imports. */
  public static List<Import> getImports(DSLContext ctx) {
    return ctx.fetch(IMPORT)
        .into(de.samply.mdr.dal.jooq.tables.pojos.Import.class).stream().map(Import::new)
        .collect(Collectors.toList());
  }

  /** Returns the import with the given id. */
  public static Import getImportById(DSLContext ctx, int id) {
    return new Import(ctx.fetchOne(IMPORT, IMPORT.ID.eq(id))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Import.class));
  }

  /** Returns all imports from a certain user. */
  public static List<Import> getImportsFromUser(DSLContext ctx, int userId) {
    return ctx.fetch(IMPORT, IMPORT.CREATED_BY.eq(userId))
        .into(de.samply.mdr.dal.jooq.tables.pojos.Import.class).stream().map(Import::new)
        .collect(Collectors.toList());
  }

  /**
   * Saves the given import.
   *
   * @return database id of the newly inserted import
   */
  public static int saveImport(DSLContext ctx, Import anImport) {
    if (anImport.getUuid() == null) {
      anImport.setUuid(UUID.randomUUID());
    }

    ImportRecord record = ctx.newRecord(IMPORT, anImport.toJooq());
    return ctx.insertInto(IMPORT).set(record).returning(IMPORT.ID).fetchOne().getId();
  }

  /** Updates the given import. */
  public static void updateImport(DSLContext ctx, Import anImport) {
    ImportRecord record = ctx.newRecord(IMPORT, anImport.toJooq());
    ctx.executeUpdate(record);
  }

  /** Deletes the given import. */
  public static void deleteImport(DSLContext ctx, Import anImport) {
    deleteImportById(ctx, anImport.getId());
  }

  /** Deletes the import with the given id. */
  public static void deleteImportById(DSLContext ctx, int id) {
    ctx.fetchOne(IMPORT, IMPORT.ID.eq(id)).delete();
  }

  /**
   * Deletes the collection of imports with the given ids.
   *
   * @param ids a list of ids to delete
   */
  public static void deleteImportsById(DSLContext ctx, Collection<Integer> ids) {
    ctx.fetch(IMPORT, IMPORT.ID.in(ids)).forEach(ImportRecord::delete);
  }

  /** Returns all imports from a certain user and namespace. */
  public static List<Import> getImportsFromUserAndNamespace(
      DSLContext ctx, int userId, int namespaceId) {
    return ctx.selectDistinct(IMPORT.fields())
        .from(IMPORT)
        .leftJoin(USER_NAMESPACE_GRANTS)
        .on(USER_NAMESPACE_GRANTS.NAMESPACE_ID.eq(IMPORT.NAMESPACE_ID))
        .where(IMPORT.NAMESPACE_ID.eq(namespaceId))
        .and(USER_NAMESPACE_GRANTS.USER_ID.eq(userId))
        .and(USER_NAMESPACE_GRANTS.GRANT_TYPE.in(GrantType.ADMIN, GrantType.WRITE))
        .orderBy(IMPORT.CREATED_AT.desc())
        .fetch().into(de.samply.mdr.dal.jooq.tables.pojos.Import.class)
        .stream().map(Import::new).collect(Collectors.toList());
  }

  /**
   * Get the ids of all namespaces this user has imports in.
   *
   * @param userId the id of the user to check
   * @return a list of namespace ids
   */
  public static List<Integer> getNamespaceIdsForUser(DSLContext ctx, int userId) {
    return ctx.selectDistinct(IMPORT.NAMESPACE_ID)
        .from(IMPORT)
        .leftJoin(USER_NAMESPACE_GRANTS)
        .on(USER_NAMESPACE_GRANTS.NAMESPACE_ID.eq(IMPORT.NAMESPACE_ID))
        .where(USER_NAMESPACE_GRANTS.USER_ID.eq(userId))
        .and(USER_NAMESPACE_GRANTS.GRANT_TYPE.in(GrantType.ADMIN, GrantType.WRITE))
        .fetch().stream().map(Record1::value1).collect(Collectors.toList());
  }

}
