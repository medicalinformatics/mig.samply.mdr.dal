/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.SCOPEDIDENTIFIER;
import static de.samply.mdr.dal.jooq.Tables.SLOT;

import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.jooq.tables.Element;
import de.samply.mdr.dal.jooq.tables.Scopedidentifier;
import de.samply.mdr.dal.jooq.tables.records.SlotRecord;
import java.util.List;
import java.util.stream.Collectors;
import org.jooq.DSLContext;

/** DAO for slots. */
public class SlotDao {

  /**
   * Returns the slots for the given URN.
   */
  public static List<Slot> getSlots(DSLContext ctx, String urn) {
    ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);
    de.samply.mdr.dal.jooq.tables.Slot slot = SLOT.as("slot");
    Scopedidentifier si = SCOPEDIDENTIFIER.as("si");
    Element ns = ELEMENT.as("ns");

    return ctx.select()
        .from(slot)
        .leftJoin(si).on(slot.SCOPEDIDENTIFIERID.eq(si.ID))
        .leftJoin(ns).on(ns.ID.eq(si.NAMESPACEID))
        .where(si.IDENTIFIER.eq(identifier.getIdentifier()))
        .and(si.VERSION.eq(identifier.getVersion()))
        .and(si.ELEMENTTYPE.eq(identifier.getElementType()))
        .and(ns.NAME.eq(identifier.getNamespace()))
        .fetch().into(slot).into(de.samply.mdr.dal.jooq.tables.pojos.Slot.class)
        .stream().map(Slot::new).collect(Collectors.toList());
  }

  /**
   * Delete slots by their ScopedIdentifierId.
   */
  public static void deleteSlotByScopedIdentifierId(DSLContext ctx, int scopedIdentifierId) {
    ctx.deleteFrom(SLOT).where(SLOT.SCOPEDIDENTIFIERID.eq(scopedIdentifierId)).execute();
  }

  /**
   * Saves the given slots.
   */
  public static void saveSlot(DSLContext ctx, Slot slot) {
    SlotRecord record = ctx.newRecord(SLOT, slot);
    ctx.executeInsert(record);
  }
}
