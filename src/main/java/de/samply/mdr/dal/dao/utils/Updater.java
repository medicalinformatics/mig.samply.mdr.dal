/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao.utils;

import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.ScopedIdentifierHierarchyDao;
import de.samply.mdr.dal.dao.SlotDao;
import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jooq.DSLContext;

/** A helper class that is used to update a group. */
public class Updater {

  private final String urn;

  private final int userId;

  private final HashMap<IdentifiedElement, IdentifiedElement> rootReplacements;

  /**
   * TODO: add javadoc.
   */
  public Updater(String urn, int userId) {
    this.urn = urn;
    this.userId = userId;
    this.rootReplacements = new HashMap<>();
  }

  public IdentifiedElement update(DSLContext ctx) {
    return updateElement(ctx, urn);
  }

  /** Updates the given URN. That means this method searches for new identifiers for elements. */
  private IdentifiedElement updateElement(DSLContext ctx, String urn) {
    IdentifiedElement element = IdentifiedDao.getElement(ctx, userId, urn);

    if (element.getScoped().getStatus() == Status.OUTDATED) {
      element = IdentifiedDao.getLatestElement(ctx, userId, urn);

      if (element == null) {
        return null;
      }
    }

    if (element.getElementType() != Elementtype.DATAELEMENTGROUP) {
      return element;
    }

    HashMap<IdentifiedElement, IdentifiedElement> replacer = new HashMap<>();

    List<IdentifiedElement> subMembers = IdentifiedDao.getSubMembers(ctx, userId, urn);

    for (IdentifiedElement subMember : subMembers) {
      IdentifiedElement newMember = updateElement(ctx, subMember.getScoped().toString());

      if (newMember == null) {
        replacer.put(subMember, null);
      } else if (newMember.getScoped().getId() != subMember.getScoped().getId()) {
        replacer.put(subMember, newMember);
      }
    }

    if (replacer.size() == 0) {
      return element;
    } else {
      rootReplacements.putAll(replacer);

      /* Create a new group. */
      DataElementGroup group = new DataElementGroup();
      ElementDao.saveElement(ctx, userId, group);

      ScopedIdentifier identifier = new ScopedIdentifier();
      identifier.setElementType(Elementtype.DATAELEMENTGROUP);
      identifier.setNamespace(element.getScoped().getNamespace());
      identifier.setNamespaceId(element.getScoped().getNamespaceId());
      identifier.setIdentifier(element.getScoped().getIdentifier());
      identifier.setVersion("" + (Integer.parseInt(element.getScoped().getVersion()) + 1));
      identifier.setElementId(group.getId());
      identifier.setCreatedBy(userId);
      identifier.setUrl("none");
      identifier.setStatus(Status.RELEASED);

      identifier.setId(ScopedIdentifierDao.saveScopedIdentifier(ctx, identifier));

      /* Copy the definitions. */
      for (Definition def : element.getDefinitions()) {
        Definition newDef = new Definition();
        newDef.setDefinition(def.getDefinition());
        newDef.setDesignation(def.getDesignation());
        newDef.setElementId(group.getId());
        newDef.setScopedIdentifierId(identifier.getId());
        newDef.setLanguage(def.getLanguage());
        DefinitionDao.saveDefinition(ctx, newDef);
      }

      /* Copy the slots. */
      for (Slot s : SlotDao.getSlots(ctx, element.getScoped().toString())) {
        Slot slot = new Slot();
        slot.setValue(s.getValue());
        slot.setKey(s.getKey());
        slot.setScopedIdentifierId(identifier.getId());
        SlotDao.saveSlot(ctx, slot);
      }

      /* Copy the elements. */
      int index = 1;
      for (IdentifiedElement subMember : subMembers) {
        if (replacer.containsKey(subMember)) {
          if (replacer.get(subMember) != null) {
            ScopedIdentifierHierarchyDao.addSubIdentifier(
                ctx, identifier.getId(), replacer.get(subMember).getScoped().getId(), index++);
          }
        } else {
          ScopedIdentifierHierarchyDao
              .addSubIdentifier(ctx, identifier.getId(), subMember.getScoped().getId(), index++);
        }
      }

      ScopedIdentifierDao.outdateIdentifier(ctx, userId, element.getScoped().toString());

      return IdentifiedDao.getElement(ctx, userId, identifier.toString());
    }
  }

  public Map<IdentifiedElement, IdentifiedElement> getRootReplacements() {
    return Collections.unmodifiableMap(rootReplacements);
  }
}
