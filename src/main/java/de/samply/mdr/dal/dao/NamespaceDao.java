/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.DEFINITION;
import static de.samply.mdr.dal.jooq.Tables.ELEMENT;

import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.GrantType;
import de.samply.mdr.dal.jooq.tables.pojos.Element;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jooq.DSLContext;
import org.jooq.JSON;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SelectConditionStep;

public class NamespaceDao {

  /**
   * Updates the given namespace with the given definitions.
   */
  public static void updateNamespace(DSLContext ctx, Namespace ns, List<Definition> definitions) {
    DefinitionDao.deleteDefinitionsByElementId(ctx, ns.getId());
    for (Definition definition : definitions) {
      definition.setElementId(ns.getId());
      DefinitionDao.saveNamespaceDefinition(ctx, definition);
    }
    updateNamespace(ctx, ns);
  }

  /**
   * Updates the visibility and data of the given namespace.
   */
  public static void updateNamespace(DSLContext ctx, Namespace ns) {
    ctx.update(ELEMENT)
        .set(ELEMENT.HIDDEN, ns.getHidden())
        .set(ELEMENT.DATA, JSON.valueOf(ns.getData().toString()))
        .where(ELEMENT.ID.eq(ns.getId()))
        .execute();
  }

  /**
   * Get the default query for multiple methods in this dao.
   * Can be extended with further where conditions.
   */
  private static SelectConditionStep<Record> getNamespacesQuery(DSLContext ctx) {
    return ctx.select(ELEMENT.fields())
        .select(DEFINITION.fields())
        .from(ELEMENT)
        .leftJoin(DEFINITION)
        .on(DEFINITION.ELEMENTID.eq(ELEMENT.ID))
        .where(ELEMENT.ELEMENTTYPE.eq(Elementtype.NAMESPACE));
  }

  /**
   * Fetch and convert DescribedElements from a given query.
   */
  private static List<DescribedElement> fetchNamespaceQuery(SelectConditionStep<Record> query) {
    // TODO: There is another way with using fetchGroups to get definitions aggregated for elements
    //       however I didn't get this working correctly.
    Result<?> result = query.fetch();
    Map<Integer, DescribedElement> namespaces = new HashMap<>();

    for (Record r : result) {
      de.samply.mdr.dal.jooq.tables.pojos.Element element =
          r.into(ELEMENT.fields()).into(de.samply.mdr.dal.jooq.tables.pojos.Element.class);
      de.samply.mdr.dal.jooq.tables.pojos.Definition definition =
          r.into(DEFINITION.fields()).into(de.samply.mdr.dal.jooq.tables.pojos.Definition.class);

      if (namespaces.containsKey(element.getId())) {
        namespaces.get(element.getId()).getDefinitions().add(new Definition(definition));
      } else {
        DescribedElement describedElement = new DescribedElement();
        describedElement.setElement(new Namespace(element));
        describedElement.getDefinitions().add(new Definition(definition));
        namespaces.put(element.getId(), describedElement);
      }
    }

    return new ArrayList<>(namespaces.values());
  }

  /**
   * Returns a list of all namespaces.
   */
  public static List<DescribedElement> getNamespaces(DSLContext ctx) {
    SelectConditionStep<Record> query = getNamespacesQuery(ctx);
    return fetchNamespaceQuery(query);
  }

  /**
   * Returns the namespace with the given name.
   */
  public static DescribedElement getNamespace(DSLContext ctx, int userId, String name) {
    SelectConditionStep<Record> query = getNamespacesQuery(ctx);
    query.and(DaoUtil.accessibleByUserId(ctx, userId))
        .and(ELEMENT.NAME.eq(name));
    return fetchNamespaceQuery(query).stream().findFirst().orElse(null);
  }

  /**
   * Returns the namespace with the given id.
   */
  public static DescribedElement getNamespaceById(DSLContext ctx, int userId, int namespaceId) {
    SelectConditionStep<Record> query = getNamespacesQuery(ctx);
    query.and(DaoUtil.accessibleByUserId(ctx, userId))
        .and(ELEMENT.ID.eq(namespaceId));
    return fetchNamespaceQuery(query).stream().findFirst().orElse(null);
  }

  /**
   * Returns all readable namespaces, including the implicitly readable namespaces.
   */
  public static List<DescribedElement> getReadableNamespaces(DSLContext ctx, int userId) {
    SelectConditionStep<Record> query = getNamespacesQuery(ctx);
    query.and(DaoUtil.accessibleByUserId(ctx, userId));
    return fetchNamespaceQuery(query);
  }

  /**
   * Returns all writable namespaces (which are not hidden or which are writable for the user as
   * defined in the "user_namespace_grants" table.
   */
  public static List<DescribedElement> getWritableNamespaces(DSLContext ctx, int userId) {
    return getNamespacesByGrantType(ctx, userId, GrantType.WRITE);
  }

  /**
   * Returns a list of namespaces which the user can explicitly read (as defined in the
   * "user_namespace_grants" table).
   */
  public static List<DescribedElement> getExplicitlyReadableNamespaces(DSLContext ctx, int userId) {
    return getNamespacesByGrantType(ctx, userId, GrantType.READ);
  }

  /**
   * Returns a list of namespaces which the user has admin access to (as defined in the
   * "user_namespace_grants" table).
   */
  public static List<DescribedElement> getAdministratableNamespaces(DSLContext ctx, int userId) {
    return getNamespacesByGrantType(ctx, userId, GrantType.ADMIN);
  }

  /**
   * Returns a list of namespaces the user has the given access type to.
   *
   * @param grantType "READ", "WRITE" or "ADMIN"
   */
  public static List<DescribedElement> getNamespacesByGrantType(
      DSLContext ctx, int userId, GrantType grantType) {
    SelectConditionStep<Record> query = getNamespacesQuery(ctx);
    query.and(ELEMENT.ID.in(DaoUtil.getUserNamespaceGrantsQuery(ctx, userId, grantType)));
    return fetchNamespaceQuery(query);
  }

  /**
   * Returns a list of namespaces which match the given input text.
   */
  public static List<DescribedElement> findNamespaces(DSLContext ctx, int userId, String str) {
    String likeStr = "%" + str + "%";
    SelectConditionStep<Record> query = getNamespacesQuery(ctx);
    query.and(DaoUtil.accessibleByUserId(ctx, userId))
        .and(DEFINITION.DEFINITION_.likeIgnoreCase(likeStr)
        .or(DEFINITION.DESIGNATION.likeIgnoreCase(likeStr)));
    return fetchNamespaceQuery(query);
  }

}
