/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal.dao;

import static de.samply.mdr.dal.jooq.Tables.ELEMENT;
import static de.samply.mdr.dal.jooq.Tables.HIERARCHY;
import static de.samply.mdr.dal.jooq.Tables.USER_NAMESPACE_GRANTS;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.jooq.enums.GrantType;
import de.samply.mdr.dal.jooq.tables.Element;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;

public class DaoUtil {

  private static final String[] materializedViews = new String[] {HIERARCHY.getName()};

  /**
   * Returns a condition which checks whether a user is able to access and see a namespace or not.
   */
  public static Condition accessibleByUserId(DSLContext ctx, int userId) {
    return ELEMENT.HIDDEN.eq(false)
        .or(ELEMENT.ID.in(getUserNamespaceGrantsQuery(ctx, userId, GrantType.READ)));
  }

  /**
   * Returns a condition which checks whether a user is able to access and see a namespace or not.
   * Requires the Element table for check as parameter.
   */
  public static Condition accessibleByUserId(DSLContext ctx, int userId, Element element) {
    return element.HIDDEN.eq(false)
        .or(element.ID.in(getUserNamespaceGrantsQuery(ctx, userId, GrantType.READ)));
  }

  /**
   * Returns a condition which checks whether a user has the required grant to a namespace or not.
   */
  public static SelectConditionStep<Record1<Integer>> getUserNamespaceGrantsQuery(
      DSLContext ctx, int userId, GrantType granttype) {
    return ctx.select(USER_NAMESPACE_GRANTS.NAMESPACE_ID)
        .from(USER_NAMESPACE_GRANTS)
        .where(USER_NAMESPACE_GRANTS.USER_ID.eq(userId))
        .and(USER_NAMESPACE_GRANTS.GRANT_TYPE.eq(granttype));
  }

  /** Refreshes all materialized views. */
  public static void refreshMaterializedViews() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      for (String view : materializedViews) {
        // TODO: Maybe there is a better way to do this with JOOQ?
        ctx.execute("REFRESH MATERIALIZED VIEW " + view);
      }
    }
  }

}
