/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceManager {

  private static String URL;
  private static String USER;
  private static String PASSWORD;

  /** Logger for the resource manager. */
  private static Logger logger = LoggerFactory.getLogger(ResourceManager.class);

  private ResourceManager() {

  }

  public static synchronized void initialize(
      String host, String database, String user, String password) {
    String url = String.format("jdbc:postgresql://%s/%s", host, database);
    initialize(url, user, password);
  }

  /**
   * Set the required parameters to get connections and context.
   */
  public static synchronized void initialize(String url, String user, String password) {
    URL = url;
    USER = user;
    PASSWORD = password;
  }

  /**
   * Get a SQL Connection with the previously initialized paramters.
   */
  public static synchronized Connection getConnection() {
    try {
      return DriverManager.getConnection(URL, USER, PASSWORD);
    } catch (SQLException e) {
      logger.error("Error while retrieving database connection: " + e);
    }
    return null;
  }

  // Find out if Configuration is a resource and can be closed, probably not
  // and the connection must be closed
  //  public static synchronized Configuration getConfiguration() {
  //    return new DefaultConfiguration().set(getConnection()).set(SQLDialect.POSTGRES);
  //  }

  /**
   * Creates a DSLContext for the previously initialized parameters.
   * This is a Resource and has to be closed after use!
   * https://stackoverflow.com/questions/27773698/how-to-manage-dslcontext-in-jooq-close-connection
   */
  public static synchronized DSLContext getDslContext()
      throws IllegalArgumentException {
    return DSL.using(URL, USER, PASSWORD);
  }

  public static String getUrl() {
    return URL;
  }

  public static String getUser() {
    return USER;
  }

  public static String getPassword() {
    return PASSWORD;
  }
}
