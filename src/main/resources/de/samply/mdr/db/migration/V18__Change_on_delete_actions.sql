--
-- Copyright (C) 2020 The Samply Community
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

ALTER TABLE concept_association
    DROP CONSTRAINT concept_association_scopedIdentifier_id_fkey;
ALTER TABLE concept_association
    ADD CONSTRAINT concept_association_scopedIdentifier_id_fkey FOREIGN KEY (scopedIdentifier_id) REFERENCES "scopedIdentifier" (id) ON DELETE CASCADE ;
-- Concept association table does not actually use camel case in the column name, the other tables do, thus the quotes there

ALTER TABLE "scopedIdentifierHierarchy"
    DROP CONSTRAINT "scopedIdentifierHierarchy_subId_fkey";
ALTER TABLE "scopedIdentifierHierarchy"
    ADD CONSTRAINT "scopedIdentifierHierarchy_subId_fkey" FOREIGN KEY ("subId") REFERENCES "scopedIdentifier" (id) ON DELETE CASCADE ;

ALTER TABLE "scopedIdentifierHierarchy"
    DROP CONSTRAINT "scopedIdentifierHierarchy_superId_fkey";
ALTER TABLE "scopedIdentifierHierarchy"
    ADD CONSTRAINT "scopedIdentifierHierarchy_superId_fkey" FOREIGN KEY ("superId") REFERENCES "scopedIdentifier" (id) ON DELETE CASCADE ;

ALTER TABLE slot
    DROP CONSTRAINT "slot_scopedIdentifierId_fkey";
ALTER TABLE slot
    ADD CONSTRAINT "slot_scopedIdentifierId_fkey" FOREIGN KEY ("scopedIdentifierId") REFERENCES "scopedIdentifier" (id) ON DELETE CASCADE ;

ALTER TABLE definition
    DROP CONSTRAINT "definition_scopedIdentifierId_fkey";
ALTER TABLE definition
    ADD CONSTRAINT "definition_scopedIdentifierId_fkey" FOREIGN KEY ("scopedIdentifierId") REFERENCES "scopedIdentifier" (id) ON DELETE CASCADE ;

ALTER TABLE definition
    DROP CONSTRAINT "definition_elementId_fkey";
ALTER TABLE definition
    ADD CONSTRAINT "definition_elementId_fkey" FOREIGN KEY ("elementId") REFERENCES "element" (id) ON DELETE CASCADE ;