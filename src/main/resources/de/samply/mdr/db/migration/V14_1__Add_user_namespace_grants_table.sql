--
-- Copyright (C) 2018 The Samply Community
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

CREATE TYPE grant_type AS ENUM ('READ', 'WRITE', 'ADMIN');

CREATE TABLE user_namespace_grants (
  user_id       INTEGER NOT NULL,
  namespace_id  INTEGER NOT NULL,
  grant_type    grant_type
);

ALTER TABLE user_namespace_grants ADD CONSTRAINT user_namespace_grants_unique UNIQUE(user_id, namespace_id, grant_type);
ALTER TABLE user_namespace_grants ADD CONSTRAINT user_namespace_grants_user_fkey FOREIGN KEY (user_id) REFERENCES "mdrUser" (id) ON DELETE CASCADE;
ALTER TABLE user_namespace_grants ADD CONSTRAINT user_namespace_grants_namespace_fkey FOREIGN KEY (namespace_id) REFERENCES "element" (id) ON DELETE CASCADE;
CREATE INDEX ON user_namespace_grants (user_id);
CREATE INDEX ON user_namespace_grants (namespace_id);

INSERT INTO user_namespace_grants (user_id, namespace_id, grant_type)
SELECT "userId", "namespaceId", 'READ' FROM "userReadableNamespace"
ON CONFLICT ON CONSTRAINT user_namespace_grants_unique DO NOTHING;

INSERT INTO user_namespace_grants (user_id, namespace_id, grant_type)
SELECT "userId", "namespaceId", 'WRITE' FROM "userWritableNamespace"
ON CONFLICT ON CONSTRAINT user_namespace_grants_unique DO NOTHING;

-- Although that should never be the case...give read access to users with write access, because it wouldn't make sense otherwise
-- This is most likely never going to be an issue
INSERT INTO user_namespace_grants (user_id, namespace_id, grant_type)
SELECT "userId", "namespaceId", 'READ' FROM "userWritableNamespace"
ON CONFLICT ON CONSTRAINT user_namespace_grants_unique DO NOTHING;