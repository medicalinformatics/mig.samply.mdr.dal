--
-- Copyright (C) 2018 The Samply Community
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

CREATE TABLE staged_element (
  id serial primary key,
  created_by INTEGER NOT NULL,
  namespace_id INTEGER NOT NULL,
  data text NOT NULL,
  element_type "elementType",
  designation text
);

ALTER TABLE staged_element ADD CONSTRAINT staged_element_mdruser_fkey FOREIGN KEY ( created_by ) REFERENCES "mdrUser" ( id ) ON DELETE SET NULL;
ALTER TABLE staged_element ADD CONSTRAINT staged_element_namespace_fkey FOREIGN KEY ( namespace_id ) REFERENCES element ( id ) ON DELETE SET NULL;
COMMENT ON COLUMN staged_element.designation IS 'This column redundantly keeps the designation of the staged element in order to show an overview without having to de-serialize the whole element first';