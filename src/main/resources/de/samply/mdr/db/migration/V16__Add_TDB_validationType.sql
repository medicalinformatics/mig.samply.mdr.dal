--
-- Copyright (C) 2019 The Samply Community
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

ALTER TABLE definition ALTER COLUMN definition DROP NOT NULL;

-- Simply adding an enum value is not possible in a transaction, so this workaround is used (as suggested in
-- https://stackoverflow.com/questions/1771543/postgresql-updating-an-enum-type/10404041#10404041 )
-- rename the old enum
ALTER TYPE "validationType" RENAME TO "validationType_";
-- create the new enum
CREATE TYPE "validationType" AS ENUM ('NONE', 'BOOLEAN', 'INTEGER', 'FLOAT', 'INTEGERRANGE', 'FLOATRANGE', 'DATE',
    'DATETIME', 'TIME', 'REGEX', 'JS', 'LUA', 'TBD');
-- alter all you enum columns
ALTER TABLE "element"
    ALTER COLUMN "validationType" TYPE "validationType" USING "validationType"::text::"validationType";
-- drop the old enum
drop type "validationType_";