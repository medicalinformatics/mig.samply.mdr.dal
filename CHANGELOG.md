# Changelog Samply MDR Data Access Layer
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.0] - 2020-03-11
### Added
- trigger-based removal of unreferenced elements
- method to delete all drafts in a namespace (namespace admin only)
- method to delete all drafts a user created in a namespace
- method to delete all drafts of a user in any namespace
### Changed
- apply google java code style, renamed some methods/classes
- Permissible values are returned in the order in which they were entered into the database.

## [2.6.0] - 2019-10-18
### Added
- Method to get unreleasable elements

## [2.5.0] - 2019-09-05
### Changed
- Updated mdr.xsd to 2.3.0

## [2.4.0] - 2019-08-12
### Added
- Method for retrieving recommendations for a searched string and optionally a language
- Optionally limit languages for subcodes from catalogs

## [2.3.0] - 2019-07-15
### Changed
- Staging area shows all imports in writable namespaces for a user, not only those he created

## [2.2.0] - 2019-06-26
### Added
- add a method to get an identified (code) element by the code itself instead of its urn

## [2.1.1] - 2019-06-14
### Fixed
- When checking which nodes have children, respect the list of allowed codes for that value domain

## [2.1.0] - 2019-06-13
### Added
- Method to retrieve only direct descendants of a catalog node

## [2.0.0] - 2019-04-25
### Changed
- switch database migration from homebrew method to flyway

## [1.13.0] - 2019-04-25
### Changed
- upgrade db version nr to 17
### Added
- add a table to store information about concept association

## [1.12.0] - 2019-04-24
### Changed
- upgrade db version nr to 16
### Added
- "TBD" validation type
### Removed
- dropped not null constraint on definition->definition

## [1.11.6] - 2018-12-19
### Changed
- change group id to de.mig-samply

## [1.11.5] - 2018-12-05
### Changed
- upgrade db version nr to 14
- minimal required postgresql version: 9.5
### Added
- Wrap staged elements in an import element
- user_namespace_grants table
### Removed
- userReadableNamespaces table
- userWritableNamespaces table

## [1.11.4] - 2018-10-05
### Changed
- Add hierarchy information to staged elements
- upgrade db version nr to 13

## [1.11.3] - 2018-08-14
### Changed
- Update common-simpledao to fix a database upgrade bug

## [1.11.2] - 2018-08-06
### Removed
- Remove user rights (create namespaces, import/export, upload catalogs). This will be handled in the auth service

## [1.11.1] - 2018-07-25
### Changed
- Update to new mdr xsd lib. Import Namespace no longer mandatory in elements.

## [1.11.0] - 2018-07-20
### Added
- Table to keep staged elements
### Changed
- Upgrade parent.pom to 9.5

## [1.10.4] - 2018-04-09
### Changed
- Change format of changelog
### Fixed
- Don't allow to ignore old revision numbers on import since this could cause mixups like older revisions of an element
having higher revision numbers than the newer revisions

## [1.10.3] - 2017-01-04
### Added
- The updater now also copies the slots

## [1.10.2] - 2016-10-07
### Fixed
- The importer now properly updates the status of existing scoped identifiers, if the importer is set to keep the
  the identifiers and versions
- Fixed a bug in the upgrade mechanism that was caused with the newest SDAO version, that executed all files first and then the java code

## [1.10.0] - 2016-09-02
### Added
- extended the exporter and importer, so that it is possible to export a complete namespace,
  and also import the identifiers and versions as they are

## [1.9.1]
### Added
- configuration option to show/hide automatic translation service for definitions

## [1.9.0] - 2015-12-18
### Added
- added the findOwnedRepresentations method that returns a list of the owned
    dataelements that described the same element as the given one
- added support for external IDs (e.g. for the caDSR)
### Changed
- switched to the open source repository

## [1.8.0] - 2015-11-06
### Changed
- polished the DAOs

## [1.7.0] - 2015-06-26
### Added
- Added a new config value: if "defaultGrant" is true, new users can create
  namespaces without explicit permission from the admin
- Added a new util used to update groups and all their sub elements
- Added support for catalogs and codes
### Changed
- Restructured all classes. Removed the "identifiedItem" and "designatable" and
  combined them into one "element". There are now different kinds of elements:
  - namespace
  - dataelement
  - dataelementgroup
  - record
  - catalog
  - code
  - enumerated value domain
  - described value domain
  - catalog value domain
  - permissible value
  
  The SQL tables are now built using the "Table per hierarchy" method, meaning that
  all elements are stored in one table with many columns. This should result in
  faster queries because of fewer joins. This change also reduced the number of
  DAOs used. Use the ElementDAO to get elements (meaning the element without
  using a scoped identifier, thus without definitions), use the IdentifiedDao to
  get namespaces (because they are somewhat special) and elements using a
  scoped identifier.

